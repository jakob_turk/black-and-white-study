﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [HideInInspector] public float move_direction_;
    [HideInInspector] public float strafe_direction_;
    [HideInInspector] public float move_speed_;
    [HideInInspector] public float rotation_x_;
    [HideInInspector] public float rotation_y_;
    [HideInInspector] public bool shooting_;
    [HideInInspector] public bool walking_ = false;
    [HideInInspector] public bool movement_pressed_ = false;
    [HideInInspector] public bool movement_pressed_true_ = false;

    [Header("Movement vars")]
    [SerializeField] private float mouse_sensitivity_min_ = 10f;
    [SerializeField] private float mouse_sensitivity_max_ = 40f;
    [SerializeField] private float speed_acceleration_;
    [SerializeField] private float to_idle_time_;

    public delegate void OnKeyPressed();
    public OnKeyPressed OnCrouch;
    public OnKeyPressed OnEquipTorch;
    public OnKeyPressed OnEquipBow;
    public OnKeyPressed OnSwapCamera;
    public OnKeyPressed OnEsc;
    public OnKeyPressed OnAnyKey;

    private float to_idle_timer_;
    private float mouse_sensitivity_;

    public static InputManager Instance;

    private void Awake() {
        if (Instance != null) {
            Destroy(gameObject);
        } else {
            Instance = this;
        }

        mouse_sensitivity_ = Mathf.Lerp(mouse_sensitivity_min_, mouse_sensitivity_max_, 0.5f);
    }

    // Update is called once per frame
    void Update() {
        bool movement_pressed_new = false;
        if (Input.GetKey(KeyCode.W) ||
            Input.GetKey(KeyCode.S) ||
            Input.GetKey(KeyCode.D) ||
            Input.GetKey(KeyCode.A)) {
            movement_pressed_new = true;
        }

        if (movement_pressed_new != movement_pressed_) {
            to_idle_timer_ += Time.deltaTime;
            if (to_idle_timer_ > to_idle_time_) {
                to_idle_timer_ = 0f;
                movement_pressed_ = movement_pressed_new;
            }
        } else {
            to_idle_timer_ = 0f;
        }
        movement_pressed_true_ = movement_pressed_new;

        move_direction_ = Input.GetAxis("Vertical");
        strafe_direction_ = Input.GetAxis("Horizontal");
        move_speed_ = Input.GetAxis("MoveSpeed") + 2f;
        walking_ = Input.GetKey(KeyCode.LeftShift);
        shooting_ = Input.GetKey(KeyCode.Mouse0);
        rotation_x_ = -Input.GetAxis("Mouse Y") * mouse_sensitivity_;
        rotation_y_ = Input.GetAxis("Mouse X") * mouse_sensitivity_;

        if (Input.GetKeyDown(KeyCode.C)) {
            OnCrouch?.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.E)) {
            OnEquipTorch?.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.Q)) {
            OnEquipBow?.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.F1)) {
            OnSwapCamera?.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            OnEsc?.Invoke();
        }
        if (Input.anyKey) {
            OnAnyKey?.Invoke();
        }
    }

    public static int GetMoveKeysPressed() {
        int num = 0;
        if (Input.GetKeyDown(KeyCode.W)) num++;
        if (Input.GetKeyDown(KeyCode.A)) num++;
        if (Input.GetKeyDown(KeyCode.S)) num++;
        if (Input.GetKeyDown(KeyCode.D)) num++;
        return num;
    }

    public static bool GetSprintKeyPressed() {
        return Input.GetKeyDown(KeyCode.LeftControl);
    }

    public float GetMouseSensitivityNormalized() {
        Debug.Log((mouse_sensitivity_ - mouse_sensitivity_min_) / (mouse_sensitivity_max_ - mouse_sensitivity_min_));
        return (mouse_sensitivity_ - mouse_sensitivity_min_) / (mouse_sensitivity_max_ - mouse_sensitivity_min_);
    }

    public void SetMouseSensitivity(float norm) {
        mouse_sensitivity_ = Mathf.Lerp(mouse_sensitivity_min_, mouse_sensitivity_max_, norm);
        Debug.Log(mouse_sensitivity_);
    }
}
