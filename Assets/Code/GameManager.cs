﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private List<CheckpointController> checkpoints_;

    private CheckpointController current_checkpoint_;

    private static GameManager gameManager;

    private void Awake() {
        if (gameManager != null) {
            Destroy(gameObject);
        } else {
            gameManager = this;
        }
    }

    public static void SetCurrentCheckpoint(CheckpointController checkpoint) {
        if (gameManager.current_checkpoint_ == null || checkpoint.checkpoint_number_ > gameManager.current_checkpoint_.checkpoint_number_) {
            gameManager.current_checkpoint_ = checkpoint;
        }
    }

    public static Transform GetCurrentCheckpoint() {
        return gameManager.current_checkpoint_.transform;
    }

    public static bool JumpToCheckpointDebug(int checkpoint) {
        if (gameManager.checkpoints_.Count <= checkpoint) {
            return false;
        }
        gameManager.current_checkpoint_ = null;

        SetCurrentCheckpoint(gameManager.checkpoints_[checkpoint]);
        
        foreach (CheckpointController chp in gameManager.checkpoints_) {
            if (chp.checkpoint_number_ <= gameManager.current_checkpoint_.checkpoint_number_) {
                chp.TriggerOnEnterEvents();
                chp.TriggerDebugEvents();
            } else {
                break;
            }
        }

        CharacterController.Respawn();
        return true;
    }

    public static void Respawn() {
        gameManager.current_checkpoint_.TriggerRespawnEvents();
    }

    public static void End() {
        UiController.GetTutorialController().TriggerStep(TutorialStep.End);
        InputManager.Instance.OnAnyKey += EndCredits;
    }

    public static void EndCredits() {
        InputManager.Instance.OnAnyKey -= EndCredits;

        UiController.ShowCredits(true);
        UiController.GetTutorialController().ReportStepDone(TutorialStep.End);

        InputManager.Instance.OnAnyKey += ExitGame;
    }

    public static void ExitGame() {
        InputManager.Instance.OnAnyKey -= ExitGame;

        Application.Quit();
    }
}
