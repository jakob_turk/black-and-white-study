﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TutorialStepData", menuName = "BWStudy/TutorialStepData")]
public class TutorialStepData : ScriptableObject
{
    public TutorialStep step;
    public string msg;
}
