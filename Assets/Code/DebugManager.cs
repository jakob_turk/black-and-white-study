﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugManager : MonoBehaviour
{
    bool jump_to_checkpoint_ = false;
    string checkpoint_to_jump_to_;

    private void Update() {
        if (Input.GetKeyDown(KeyCode.F2)) {
            if (Time.timeScale == 0f) {
                return;
            }
            Time.timeScale -= Time.timeScale > 1f ? 0.5f : 0.2f;
            Debug.Log("TimeScale changed: " + Time.timeScale);
        }
        if (Input.GetKeyDown(KeyCode.F3)) {
            Time.timeScale += Time.timeScale < 1f ? 0.2f : 0.5f;
            Debug.Log("TimeScale changed: " + Time.timeScale);
        }
        if (Input.GetKeyDown(KeyCode.F3)) {
            Time.timeScale += Time.timeScale < 1f ? 0.2f : 0.5f;
            Debug.Log("TimeScale changed: " + Time.timeScale);
        }
        if (Input.GetKeyDown(KeyCode.F4)) {
            CharacterController.Respawn();
        }
        if (Input.GetKeyDown(KeyCode.F5)) {
            jump_to_checkpoint_ = !jump_to_checkpoint_;
            if (jump_to_checkpoint_) {
                Debug.Log("Input checkpoint to jump to:");
                checkpoint_to_jump_to_ = string.Empty;
            } else {
                if (GameManager.JumpToCheckpointDebug(int.Parse(checkpoint_to_jump_to_))) {
                    Debug.Log("Jumping to checkpoint " + checkpoint_to_jump_to_);
                } else {
                    Debug.Log("Checpoint " + checkpoint_to_jump_to_ + " does not exist");
                }
            }
        }
        if (jump_to_checkpoint_) {
            for (int i = 0; i < 10; i++) {
                if (Input.GetKeyDown(i.ToString())) {
                    checkpoint_to_jump_to_ += i.ToString();
                }
            }
        }
    }
}
