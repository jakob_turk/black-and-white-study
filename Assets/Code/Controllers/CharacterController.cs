﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;

public enum PlayerState { Active, InMenu, Dead, RPing }

public class CharacterController : MonoBehaviour
{
    [SerializeField] private CharacterAnimationController animation_controller_;
    [SerializeField] private CharacterEquipmentController equipment_controller_;
    [SerializeField] private CameraController camera_controller_;
    [SerializeField] private Animator animator_;
    [SerializeField] private CapsuleCollider collider_;
    [SerializeField] private Rigidbody rigidbody_;

    [SerializeField] private float aim_weight_speed_;
    [SerializeField] private float max_angular_velocity_;

    private static CharacterController Player;

    private PlayerState state_ = PlayerState.Active;
    private PlayerState pre_pause_state_ = PlayerState.Active;
    
    private bool falling_ = false;
    private bool shooting_ = false;
    
    private float aim_weight_;

    private float death_timer_ = -1f;
    private float death_respawn_time_ = 2.75f;

    private void Awake() {
        if (Player != null) {
            Destroy(gameObject);
        } else {
            Player = this;
        }
    }

    void Start() {
        Cursor.lockState = CursorLockMode.Locked;

        equipment_controller_.Init(false);
        equipment_controller_.EquipBow();

        InputManager.Instance.OnEquipTorch += EquipTorch;
        InputManager.Instance.OnEquipBow += EquipBow;
        InputManager.Instance.OnCrouch += Crouch;
    }

    private void OnDestroy() {
        InputManager.Instance.OnEquipTorch -= EquipTorch;
        InputManager.Instance.OnEquipBow -= EquipBow;
        InputManager.Instance.OnCrouch -= Crouch;
    }

    void Update() {
        if (death_timer_ >= 0f) {
            death_timer_ += Time.deltaTime;
            if (death_timer_ > death_respawn_time_) {
                Respawn();
            }
        }

        if (state_ == PlayerState.Dead) {
            return;
        }

        // Free fall
        RaycastHit hit;
        bool floored = Physics.SphereCast(transform.position + collider_.center,
                                collider_.radius * 0.5f, -1 * transform.up, out hit, collider_.height * 0.5f + collider_.radius, ~LayerMask.GetMask("CharacterTrigger"));
        if (!floored && !falling_) {
            animator_.ResetTrigger("Land");
            animator_.SetTrigger("ToFalling");
            falling_ = true;
        } else if (floored && falling_) {
            animator_.SetTrigger("Land");
            falling_ = false;
        }

        if (floored && hit.transform.tag == "Syphon") {
            // Quick fix for too frictiony syphon
            Vector3 normal = hit.normal;
            rigidbody_.AddForce(new Vector3(normal.x, -normal.z, normal.z) * 5f, ForceMode.Impulse);
        }
        if (floored && hit.transform.tag == "MovingPlatform") {
            if (transform.parent != hit.transform) {
                transform.SetParent(hit.transform, true);
            }
        } else {
            if (transform.parent != null) {
                transform.parent = null;
            }
        }

        // Turning
        float turn_angle = (transform.rotation * Quaternion.Inverse(camera_controller_.camera_rotator_.rotation)).eulerAngles.y;
        if (turn_angle > 180f)
            turn_angle -= 360f;

        if (InputManager.Instance.movement_pressed_true_) {
            animator_.SetFloat("TurnSpeed", 0);
        } else {
            animator_.SetFloat("TurnSpeed", Mathf.Clamp(-turn_angle / 90f, -1f, 1f));
        }
        
        // Shooting
        if (aim_weight_ > 0f && !shooting_) {
            equipment_controller_.DropArrow();
        }
        aim_weight_ += (shooting_ ? 1f : -1f) * aim_weight_speed_ * Time.deltaTime;
        aim_weight_ = Mathf.Clamp01(aim_weight_);
        animation_controller_.aim_weight_ = aim_weight_;

        // Animator
        animator_.SetLayerWeight(animator_.GetLayerIndex("Aim layer"), aim_weight_);
        animator_.SetFloat("MoveSpeed", InputManager.Instance.move_direction_ * InputManager.Instance.move_speed_);
        animator_.SetFloat("StrafeSpeed", InputManager.Instance.strafe_direction_ * InputManager.Instance.move_speed_);
        animator_.SetBool("Idle", (InputManager.Instance.move_direction_ == 0f && InputManager.Instance.strafe_direction_ == 0f) || falling_);
        animator_.SetBool("Walking", InputManager.Instance.walking_);
        animator_.SetBool("NothingPressed", !InputManager.Instance.movement_pressed_);
        if (equipment_controller_.BowEquipped && state_ == PlayerState.Active) {
            animator_.SetBool("Aiming", InputManager.Instance.shooting_);
        }

        // Misc
        if (InputManager.Instance.movement_pressed_true_) {
            AnalyticsManager.ReportMoveUsage(InputManager.GetMoveKeysPressed());
            UiController.GetTutorialController().ReportStepDone(TutorialStep.Move);
            if (InputManager.GetSprintKeyPressed()) {
                AnalyticsManager.ReportSprintUsage();
                UiController.GetTutorialController().ReportStepDone(TutorialStep.Sprint);
            }
        }
    }

    private void FixedUpdate() {
        if (state_ == PlayerState.InMenu) {
            return;
        }

        camera_controller_.UpdateCamera();

        if (state_ == PlayerState.RPing) {
            return;
        }

        Quaternion old_cam_rotation = camera_controller_.camera_rotator_.rotation;

        float turn_angle = (transform.rotation * Quaternion.Inverse(camera_controller_.camera_rotator_.rotation)).eulerAngles.y;
        if (turn_angle > 180f)
            turn_angle -= 360f;
        if (InputManager.Instance.movement_pressed_true_) {
            if (!falling_) {
                float max_turn_angle = max_angular_velocity_ * Time.fixedDeltaTime;
                turn_angle = Mathf.Clamp(turn_angle, -max_turn_angle, max_turn_angle);
                transform.Rotate(new Vector3(0f, -turn_angle, 0f));
            }
        }

        camera_controller_.camera_rotator_.rotation = old_cam_rotation;
    }

    private void Crouch() {
        animator_.SetBool("Crouched", !animator_.GetBool("Crouched"));
    }

    private void EquipTorch() {
        if (state_ != PlayerState.Active) {
            return;
        }
        if (!equipment_controller_.HasTorch) {
            Collider[] triggers = Physics.OverlapSphere(transform.position, 3f, LayerMask.GetMask("CharacterTrigger"), QueryTriggerInteraction.Collide);
            foreach (Collider trigger in triggers) {
                if (trigger.tag == "Torch") {
                    TorchPostController post = trigger.GetComponent<TorchPostController>();
                    post.ForceEvent(3f);

                    state_ = PlayerState.RPing;
                    animation_controller_.prevent_look_at_ = true;

                    animator_.SetTrigger("EquipTorchWall");
                    equipment_controller_.RegisterTorch(post.torch_);
                    
                    Vector3 look_direction = post.torch_.transform.position - transform.position;
                    Vector3 look_direction_xz = new Vector3(look_direction.x, 0f, look_direction.z);
                    gameObject.Tween("rp_rotation", transform.rotation, Quaternion.LookRotation(look_direction_xz, Vector3.up) * Quaternion.Euler(0f, 5f, 0f),
                        1f, TweenScaleFunctions.QuadraticEaseInOut, (p) => { transform.rotation = p.CurrentValue; });
                    gameObject.Tween("rp_position", transform.position, transform.position + look_direction_xz.normalized * (look_direction_xz.magnitude - 1.1f),
                        1.2f, TweenScaleFunctions.QuadraticEaseInOut, (p) => { transform.position = p.CurrentValue; });
                    break;
                }
            }

            UiController.GetTutorialController().ReportStepDone(TutorialStep.PickUpTorch);
        } else {
            Collider[] triggers = Physics.OverlapSphere(transform.position, 1f, LayerMask.GetMask("CharacterTrigger"), QueryTriggerInteraction.Collide);
            foreach (Collider trigger in triggers) {
                if (trigger.tag == "Firewood") {
                    state_ = PlayerState.RPing;
                    animation_controller_.prevent_look_at_ = true;

                    animator_.SetTrigger("UnequipTorchFloor");

                    Vector3 look_direction = trigger.transform.position - transform.position;
                    Vector3 look_direction_xz = new Vector3(look_direction.x, 0f, look_direction.z);
                    gameObject.Tween("rp_rotation", transform.rotation, Quaternion.LookRotation(look_direction_xz, Vector3.up) * Quaternion.Euler(0f, 5f, 0f),
                        1f, TweenScaleFunctions.QuadraticEaseInOut, (p) => { transform.rotation = p.CurrentValue; });
                    gameObject.Tween("rp_position", transform.position, transform.position + look_direction_xz.normalized * (look_direction_xz.magnitude - 1.3f),
                        1.2f, TweenScaleFunctions.QuadraticEaseInOut, (p) => { transform.position = p.CurrentValue; });
                    break;
                }
            }

            equipment_controller_.EquipTorch();
            UiController.GetTutorialController().ReportStepDone(TutorialStep.LightTheFire);
        }
    }

    private void EquipBow() {
        if (state_ != PlayerState.Active) {
            return;
        }
        Collider[] triggers = Physics.OverlapSphere(transform.position, 1f, LayerMask.GetMask("CharacterTrigger"), QueryTriggerInteraction.Collide);
        foreach (Collider trigger in triggers) {
            if (trigger.tag == "TunnelTrigger") {
                ScriptedEventController event_trigger = trigger.GetComponent<ScriptedEventController>();
                event_trigger.TryTriggerEvent();
                break;
            }
        }

        equipment_controller_.EquipBow();
    }

    public void EndRp() {
        state_ = PlayerState.Active;
        animation_controller_.prevent_look_at_ = false;
    }

    public static void SetShooting() {
        Player.shooting_ = true;
    }

    public void SetNotShooting() {
        shooting_ = false;
    }

    private void OnCollisionEnter(Collision collision) {
        if (death_timer_ < 0f && (collision.gameObject.layer == LayerMask.NameToLayer("Enemy") || collision.gameObject.layer == LayerMask.NameToLayer("EnemyAdditional"))) {
            KnightController knight = collision.gameObject.GetComponent<KnightController>();
            if (knight != null && knight.State != EnemyState.Dead) {
                knight.OnKill();
                Die();
            }
        }
    }

    public static void Die(bool free_fall = false) {
        if (!free_fall) {
            Player.animator_.SetTrigger("Die");
        }
        Player.camera_controller_.Freeze();
        Player.death_timer_ = 0f;
        UiController.TriggerDeathUi();

        Player.state_ = PlayerState.Dead;
    }

    public static void Respawn() {
        Transform checkpoint = GameManager.GetCurrentCheckpoint();
        GameManager.Respawn();
        Player.transform.position = checkpoint.position;
        Player.transform.rotation = checkpoint.rotation;
        Player.camera_controller_.ResetCamera();
        Player.animator_.SetTrigger("Respawn");
        Player.death_timer_ = -1f;

        if (Player.state_ != PlayerState.InMenu) {
            Player.state_ = PlayerState.Active;
        }
    }

    public void EndGame() {
        state_ = PlayerState.RPing;
        camera_controller_.EndSequence1();
        animator_.SetTrigger("End");
    }

    public void ReachTopOfPlatform() {
        gameObject.Tween("character_pos_tweak", transform.localPosition, new Vector3(0f, transform.localPosition.y, 0f), 4f, TweenScaleFunctions.QuadraticEaseInOut,
            (p) => {
                transform.localPosition = p.CurrentValue;
            });
        camera_controller_.EndSequence3();
        animation_controller_.prevent_look_at_ = true;

        Invoke("TriggerEnd", 20f);
    }

    public static void EnterMenu() {
        Player.pre_pause_state_ = Player.state_;
        Player.state_ = PlayerState.InMenu;

        Cursor.lockState = CursorLockMode.None;
    }

    public static void ExitMenu() {
        Player.state_ = Player.pre_pause_state_;

        Cursor.lockState = CursorLockMode.Locked;
    }

    public static bool IsInMenu() {
        return Player.state_ == PlayerState.InMenu;
    }

    private void TriggerEnd() {
        GameManager.End();
    }

    public static CharacterEquipmentController GetEquipmentController() {
        return Player.equipment_controller_;
    }

    public static Collider GetCollider() {
        return Player.collider_;
    }

    public static Transform GetCamera() {
        return Player.camera_controller_.GetCamera().transform;
    }

    public static bool IsFalling() {
        return Player.falling_;
    }
}
