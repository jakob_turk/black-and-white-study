﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDelegateController : MonoBehaviour
{
    public delegate void OnCollision(Collider collision);
    public OnCollision onCollision;
    public OnCollision onExitTrigger;

    private void OnTriggerEnter(Collider other) {
        onCollision?.Invoke(other);
    }

    private void OnTriggerExit(Collider other) {
        onExitTrigger?.Invoke(other);
    }
}
