﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;

public class CameraController : MonoBehaviour
{
    enum CameraState { Aiming, Lerping, Following, End1, End2 };
    
    [SerializeField] public Transform camera_rotator_;
    [SerializeField] public Transform camera_holder_;
    [SerializeField] private Camera cameraObraDin_;
    [SerializeField] private Camera cameraRoystan_;
    [SerializeField] private Transform head_position_;
    [SerializeField] private Transform locator_elevator_;
    [SerializeField] private Transform locator_end_;
    [SerializeField] private List<Renderer> erika_materials_;
    [SerializeField] private float far_dissolve_clip_;
    private bool shadows_on_ = true;

    [HideInInspector] private float camera_x_rotation_;
    [HideInInspector] private float camera_y_rotation_;

    private CameraState state_ = CameraState.Aiming;
    private Quaternion frozen_rotation_;
    private float lerp_t_;
    
    private Vector3 default_holder_position_;
    private Quaternion default_holder_rotation_;

    private void Awake() {
        default_holder_position_ = camera_holder_.localPosition;
        default_holder_rotation_ = camera_holder_.localRotation;
    }

    private void Start() {
        SetCamera(cameraRoystan_.gameObject.activeSelf);

        InputManager.Instance.OnSwapCamera += SwapCamera;
    }

    private void OnDestroy() {
        InputManager.Instance.OnSwapCamera -= SwapCamera;
    }

    private void Update() {
        float dist = (head_position_.position - camera_holder_.position).magnitude;
        float disolve_treshold = (1f - (dist - GetCamera().nearClipPlane * 2f) / far_dissolve_clip_);
        foreach (Renderer mesh in erika_materials_) {
            mesh.material.SetFloat("_DissolveTreshold", disolve_treshold);
            if (disolve_treshold > 0.6f && shadows_on_) {
                mesh.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                mesh.receiveShadows = false;
            } else if (disolve_treshold <= 0.6f && !shadows_on_) {
                mesh.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                mesh.receiveShadows = true;
            }
        }
        shadows_on_ = disolve_treshold <= 0.6f;

        if (state_ != CameraState.Aiming) {
            return;
        }
        
        RaycastHit hit;
        Vector3 cast_direction = (camera_holder_.position - head_position_.position).normalized;
        float radius = 0.3f;
        if (Physics.SphereCast(head_position_.position, radius, cast_direction, out hit, 2.24f + radius, ~LayerMask.GetMask("Arrow", "CharacterTrigger", "EnemyAdditional", "Light", "Enemy"))) {
            camera_holder_.position = head_position_.position + cast_direction * hit.distance;
        } else {
            camera_holder_.localPosition = default_holder_position_;
        }
    }

    public void UpdateCamera() {
        switch (state_) {
        case CameraState.Aiming:
        case CameraState.End1:
            camera_x_rotation_ += InputManager.Instance.rotation_x_;
            camera_x_rotation_ = Mathf.Clamp(camera_x_rotation_, -70f, 70f);
            camera_y_rotation_ += InputManager.Instance.rotation_y_;

            camera_rotator_.rotation = Quaternion.Euler(camera_x_rotation_, camera_y_rotation_, 0f);
            break;
        case CameraState.Lerping:
            // delegate lerping to FixedUpdate I guess...does not fix the stutter completely though
            UpdateFrozenRotation(lerp_t_);
            break;
        case CameraState.Following:
            Vector3 to_target = CharacterController.GetEquipmentController().GetSpine().position - camera_holder_.position;
            camera_holder_.localRotation = Quaternion.LookRotation(to_target,
                                                                   Vector3.Cross(to_target,
                                                                                 Vector3.Cross(new Vector3(to_target.x, 0f, to_target.z),
                                                                                               to_target)));
            break;
        case CameraState.End2:
            break;
        }
    }

    private void UpdateFrozenRotation(float t) {
        Vector3 to_target = CharacterController.GetEquipmentController().GetSpine().position - camera_holder_.position;
        camera_holder_.localRotation = Quaternion.Lerp(
            frozen_rotation_,
            Quaternion.LookRotation(to_target,
                                    Vector3.Cross(to_target,
                                                  Vector3.Cross(new Vector3(to_target.x, 0f, to_target.z),
                                                                to_target))),
            t);
    }

    private void FinishFrozenLerp(ITween<float> t) {
        if (state_ == CameraState.Lerping) {
            state_ = CameraState.Following;
        }
    }

    public void Freeze() {
        state_ = CameraState.Lerping;
        camera_holder_.parent = null;

        frozen_rotation_ = camera_holder_.localRotation;
        camera_holder_.gameObject.Tween("camera_rot", 0f, 1f, 1f, TweenScaleFunctions.CubicEaseOut, (p) => { lerp_t_ = p.CurrentValue; }, FinishFrozenLerp);
    }

    public void ResetCamera() {
        state_ = CameraState.Aiming;
        camera_holder_.parent = camera_rotator_;
        camera_holder_.localPosition = default_holder_position_;
        camera_holder_.localRotation = default_holder_rotation_;
        camera_rotator_.localRotation = Quaternion.identity;
        camera_x_rotation_ = camera_rotator_.eulerAngles.x;
        camera_y_rotation_ = camera_rotator_.eulerAngles.y;
    }

    private void SetCamera(bool roystan) {
        if (roystan) {
            Debug.Log("Activating camera: Roystan");
        } else {
            Debug.Log("Activating camera: ObraDin");
        }
        cameraObraDin_.gameObject.SetActive(!roystan);
        cameraRoystan_.gameObject.SetActive(roystan);

        cameraObraDin_.tag = !roystan ? "MainCamera" : "Untagged";
        cameraRoystan_.tag = roystan ? "MainCamera" : "Untagged";
    }

    private void SwapCamera() {
        SetCamera(cameraObraDin_.gameObject.activeSelf);
    }

    public Camera GetCamera() {
        return cameraObraDin_.gameObject.activeSelf ? cameraObraDin_ : cameraRoystan_;
    }

    public void AddRenderer(Renderer renderer) {
        erika_materials_.Add(renderer);
    }

    public void EndSequence1() {
        state_ = CameraState.End1;
        camera_holder_.gameObject.Tween("camera_pos", camera_holder_.localPosition, locator_elevator_.localPosition, 4f, TweenScaleFunctions.QuadraticEaseInOut,
            (p) => {
                camera_holder_.localPosition = p.CurrentValue;
            });
        Invoke("EndSequence2", 2.5f);
    }

    public void EndSequence2() {
        state_ = CameraState.End2;
        camera_rotator_.gameObject.Tween("camera_rot", camera_rotator_.localRotation, locator_elevator_.localRotation, 8f, TweenScaleFunctions.QuadraticEaseInOut,
            (p) => {
                camera_rotator_.localRotation = p.CurrentValue;
            });
    }

    public void EndSequence3() {
        state_ = CameraState.End2;
        camera_holder_.gameObject.Tween("camera_pos", camera_holder_.localPosition, locator_end_.localPosition, 30f, TweenScaleFunctions.QuadraticEaseInOut,
            (p) => {
                camera_holder_.localPosition = p.CurrentValue;
            });
        camera_holder_.gameObject.Tween("camera_holder_rot", camera_holder_.localRotation, locator_end_.localRotation, 6f, TweenScaleFunctions.QuadraticEaseInOut,
            (p) => {
                camera_holder_.localRotation = p.CurrentValue;
            });
        camera_rotator_.gameObject.Tween("camera_rot", 0f, 1440f, 60f, TweenScaleFunctions.Linear,
            (p) => {
                camera_rotator_.localEulerAngles = new Vector3(camera_rotator_.localEulerAngles.x, p.CurrentValue, 0f);
            },
            (p) => { EndSequenceLoop(); });
    }

    public void EndSequenceLoop() {
        camera_rotator_.gameObject.Tween("camera_rot", 0f, 1440f, 60f, TweenScaleFunctions.Linear,
            (p) => {
                camera_rotator_.localEulerAngles = new Vector3(camera_rotator_.localEulerAngles.x, p.CurrentValue, 0f);
            },
            (p) => { EndSequenceLoop(); });
    }
}
