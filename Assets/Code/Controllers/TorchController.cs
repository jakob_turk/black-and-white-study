﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchController : MonoBehaviour
{
    [SerializeField] private Animator animator_;
    [SerializeField] private Collider collider_;
    [SerializeField] private Renderer mesh_;

    private bool picked_up_ = false;

    public void CallAppear(float delay) {
        Invoke("Appear", delay);
    }

    private void Appear() {
        if (picked_up_) {
            return;
        }
        picked_up_ = true;
        animator_.SetTrigger("Appear");
        animator_.SetBool("Lighted", true);
    }

    public void PickUp() {
        animator_.SetTrigger("PickUp");
        picked_up_ = true;
        collider_.enabled = true;
    }

    public void LightTorch() {
        animator_.SetBool("Lighted", true);
        picked_up_ = true;
        collider_.enabled = true;
    }

    public void PutOutTorch() {
        animator_.SetBool("Lighted", false);
        picked_up_ = true;
        collider_.enabled = false;
    }

    public void EnableCollider(bool enable) {
        collider_.enabled = enable;
    }

    public Renderer GetMesh() {
        return mesh_;
    }
}
