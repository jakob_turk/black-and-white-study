﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;

public class CharacterEquipmentController : MonoBehaviour
{
    [SerializeField] private Animator animator_;
    [SerializeField] private CameraController camera_controller_;
    [SerializeField] private Transform left_hand_addon_;
    [SerializeField] private Transform right_hand_addon_;
    [SerializeField] private Transform back_bow_addon_;
    [SerializeField] private Transform back_torch_addon_;
    [SerializeField] private Transform back_quiver_addon_;
    [SerializeField] private Transform spine_bone_;
    [SerializeField] private float arrowSpeed_;

    [Header("Prefabs")]
    [SerializeField] private ArrowController prefabArrow_;
    [SerializeField] private Transform bow_prefab_;
    [SerializeField] private TorchController torch_prefab_;

    private Transform bow_;
    private TorchController torch_;

    public bool BowEquipped { get; private set; }
    public bool TorchEquipped { get; private set; }
    public bool HasTorch { get; private set; }

    private ArrowController current_arrow_;

    public void Awake() {
        BowEquipped = false;
        TorchEquipped = false;
    }

    public void Init(bool torch) {
        if (bow_ == null) {
            bow_ = Instantiate(bow_prefab_);
            bow_.SetParent(back_bow_addon_);
            bow_.localPosition = Vector3.zero;
            bow_.localRotation = Quaternion.identity;
            camera_controller_.AddRenderer(bow_.GetComponent<Renderer>());
        }

        if (torch && torch_ == null) {
            HasTorch = torch;
            torch_ = Instantiate(torch_prefab_);
            torch_.transform.SetParent(back_torch_addon_);
            torch_.transform.localPosition = Vector3.zero;
            torch_.transform.localRotation = Quaternion.identity;
            camera_controller_.AddRenderer(torch_.GetMesh());
        }
    }

    public void EquipBow() {
        if (BowEquipped ||
            CharacterController.IsFalling()) {
            return;
        }
        animator_.SetTrigger("EquipBow");

        UiController.GetTutorialController().ReportStepDone(TutorialStep.SwapForBow);
    }

    public void RegisterTorch(TorchController torch) {
        torch_ = torch;
    }

    public void PickUpRegisteredTorch() {
        HasTorch = true;
        TorchEquipped = true;
        
        AttachTorchToHand();
        torch_.PickUp();
        camera_controller_.AddRenderer(torch_.GetMesh());
    }

    public void EquipTorch() {
        if (!HasTorch ||
            TorchEquipped ||
            CharacterController.IsFalling()) {
            return;
        }
        animator_.SetTrigger("EquipTorch");

        UiController.GetTutorialController().ReportStepDone(TutorialStep.SwapBack);
    }

    void UpdatePosition(ITween<Vector3> pos, Transform transf) {
        transf.localPosition = pos.CurrentValue;
    }
    void UpdateRotation(ITween<Quaternion> pos, Transform transf) {
        transf.localRotation = pos.CurrentValue;
    }
    
    public void AttachBowToHand() {
        bow_.SetParent(left_hand_addon_, true);
        bow_.gameObject.Tween("equip_bow_pos", bow_.localPosition, Vector3.zero, 0.2f, TweenScaleFunctions.QuadraticEaseInOut, (p) => UpdatePosition(p, bow_));
        bow_.gameObject.Tween("equip_bow_rot", bow_.localRotation, Quaternion.identity, 0.2f, TweenScaleFunctions.QuadraticEaseInOut,
            (p) => UpdateRotation(p, bow_),
            (p) => {
                animator_.SetBool("Bow", true);
                BowEquipped = true;
            });
    }
    public void AttachBowToBack() {
        bow_.SetParent(back_bow_addon_, true);
        bow_.gameObject.Tween("equip_bow_pos", bow_.localPosition, Vector3.zero, 0.2f, TweenScaleFunctions.QuadraticEaseInOut, (p) => UpdatePosition(p, bow_));
        bow_.gameObject.Tween("equip_bow_rot", bow_.localRotation, Quaternion.identity, 0.2f, TweenScaleFunctions.QuadraticEaseInOut,
            (p) => UpdateRotation(p, bow_),
            (p) => {
                animator_.SetBool("Bow", false);
                BowEquipped = false;
            });
    }
    
    public void AttachTorchToHand() {
        torch_.transform.SetParent(left_hand_addon_, true);
        torch_.gameObject.Tween("equip_torch_pos", torch_.transform.localPosition, Vector3.zero, 0.2f, TweenScaleFunctions.QuadraticEaseInOut,(p) => UpdatePosition(p, torch_.transform));
        torch_.gameObject.Tween("equip_torch_rot", torch_.transform.localRotation, Quaternion.identity, 0.2f, TweenScaleFunctions.QuadraticEaseInOut,
            (p) => UpdateRotation(p, torch_.transform),
            (p) => {
                animator_.SetBool("Torch", true);
                TorchEquipped = true;
            });
    }
    public void AttachTorchToBack() {
        torch_.transform.SetParent(back_torch_addon_, true);
        torch_.gameObject.Tween("equip_torch_pos", torch_.transform.localPosition, Vector3.zero, 0.2f, TweenScaleFunctions.QuadraticEaseInOut, (p) => UpdatePosition(p, torch_.transform));
        torch_.gameObject.Tween("equip_torch_rot", torch_.transform.localRotation, Quaternion.identity, 0.2f, TweenScaleFunctions.QuadraticEaseInOut,
            (p) => UpdateRotation(p, torch_.transform),
            (p) => {
                animator_.SetBool("Torch", false);
                TorchEquipped = false;
            });
    }
    public void LightTorch() {
        torch_.LightTorch();
    }
    public void PutOutTorch() {
        torch_.PutOutTorch();
    }
    public void DisableTorchCollider() {
        torch_.EnableCollider(false);
    }
    public void DropTorch() {
        torch_.transform.SetParent(null);
        animator_.SetBool("Torch", false);
        HasTorch = false;
        torch_ = null;

        Collider[] triggers = Physics.OverlapSphere(transform.position, 1f, LayerMask.GetMask("CharacterTrigger"), QueryTriggerInteraction.Collide);
        foreach (Collider trigger in triggers) {
            if (trigger.tag == "Firewood") {
                ScriptedEventController firewood = trigger.GetComponent<ScriptedEventController>();
                firewood.ForceEvent();
                break;
            }
        }
    }

    public void EquipArrow() {
        current_arrow_ = Instantiate(prefabArrow_, back_quiver_addon_.position, back_quiver_addon_.rotation, right_hand_addon_);
        current_arrow_.gameObject.Tween("arrow_pos", current_arrow_.transform.localPosition, Vector3.zero, 0.05f, TweenScaleFunctions.QuadraticEaseInOut,
            (p) => { if (current_arrow_ != null) { UpdatePosition(p, current_arrow_.transform); } }
        );
    }

    public void FlipArrow() {
        if (current_arrow_ != null) {
            current_arrow_.gameObject.Tween("arrow_rot", current_arrow_.transform.localRotation, Quaternion.identity, 0.1f, TweenScaleFunctions.QuadraticEaseInOut,
                (p) => { if (current_arrow_ != null) { UpdateRotation(p, current_arrow_.transform); } }
            );
        }
    }

    public void DropArrow() {
        if (current_arrow_ != null) {
            current_arrow_.DropArrow();
            current_arrow_ = null;
        }
    }

    public void Shoot() {
        if (current_arrow_ == null) {
            Debug.Log("Shoot: arrow missing");
            return;
        }
        current_arrow_.transform.parent = null;
        current_arrow_.Shoot();

        Vector3 aim_target;
        RaycastHit hit;
        Ray ray = camera_controller_.GetCamera().ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height * 11f / 20f, 0f));
        if (Physics.Raycast(ray, out hit, 100f, ~LayerMask.GetMask("CharacterTrigger", "Light"))) {
            aim_target = hit.point;
        } else {
            aim_target = camera_controller_.GetCamera().transform.position + ray.direction * 100f;
        }

        current_arrow_.transform.forward = aim_target - right_hand_addon_.position;
        current_arrow_.rigidbody_.velocity = current_arrow_.transform.forward * arrowSpeed_;
        current_arrow_ = null;

        AnalyticsManager.ReportShootUsage();
        UiController.GetTutorialController().ReportStepDone(TutorialStep.Shoot);
    }

    public Transform GetSpine() {
        return spine_bone_;
    }

    public bool HasTorchRegistered() {
        return torch_ != null;
    }
}
