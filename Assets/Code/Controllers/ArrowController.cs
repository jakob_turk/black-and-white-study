﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{
    enum ArrowState { Aiming, Flying, Hit, Stopped, Dropped };

    [SerializeField] public Animator animator_;
    [SerializeField] public Rigidbody rigidbody_;
    [SerializeField] public BoxCollider collider_;
    [SerializeField] public BoxCollider tipCollider_;
    [SerializeField] private TriggerDelegateController tip_;
    [SerializeField] private ArrowAnimationEvents anim_events_;
    [SerializeField] private float hitDrag_;
    [SerializeField] private float lifetime_;
    [SerializeField] private int max_arrows_;

    private int movableObjectLayer_;
    private Vector3 preHitPosition_ = Vector3.zero;
    private Quaternion preHitRotation_ = Quaternion.identity;
    private Vector3 velocity_ = Vector3.zero;
    private float maxSpeed_ = 0f;
    private float minSpeed_ = 0f;
    private float timer_ = 0f;
    private ArrowState state_ = ArrowState.Aiming;

    private static int max_arrows_static_ = -1;
    private static List<ArrowController> arrows_ = new List<ArrowController>();

    private void Awake() {
        if (max_arrows_static_ < 0) {
            max_arrows_static_ = max_arrows_;
        }
        arrows_.Add(this);

        if (arrows_.Count > max_arrows_static_) {
            arrows_[0].TriggerDisappear();
        }

        minSpeed_ = collider_.size.z * 0.05f / Time.fixedDeltaTime;
        maxSpeed_ = collider_.size.z * 0.5f / Time.fixedDeltaTime;

        rigidbody_.useGravity = false;
    }

    private void Start() {
        tip_.onCollision += OnCollision;
        anim_events_.onDestroyArrow += DestroyArrow;

        movableObjectLayer_ = LayerMask.NameToLayer("MovableMesh");
    }

    private void OnDestroy() {
        tip_.onCollision -= OnCollision;
        anim_events_.onDestroyArrow -= DestroyArrow;
    }

    private void Update() {
        if (state_ != ArrowState.Flying) {
            return;
        }
        timer_ += Time.deltaTime;
        if (timer_ > lifetime_) {
            DestroyArrow();
        }
    }

    private void FixedUpdate() {
        switch (state_) {
        case ArrowState.Aiming:
            transform.localPosition = Vector3.zero;
            break;
        case ArrowState.Flying:
            preHitPosition_ = transform.position;
            preHitRotation_ = transform.rotation;
            break;
        case ArrowState.Hit:
            float speed = velocity_.magnitude;
            if (speed > 0f) {
                if (speed - hitDrag_ * Time.fixedDeltaTime > 0f) {
                    velocity_ = velocity_ * (1 - (hitDrag_ * Time.fixedDeltaTime) / speed);
                    transform.position += velocity_ * Time.fixedDeltaTime;
                } else {
                    velocity_ = Vector3.zero;
                    state_ = ArrowState.Stopped;
                }
            }
            break;
        case ArrowState.Stopped:
            break;
        }
    }

    public void Shoot() {
        state_ = ArrowState.Flying;
        rigidbody_.useGravity = true;
        collider_.enabled = true;
        tipCollider_.enabled = true;
    }

    public void DropArrow() {
        state_ = ArrowState.Dropped;
        TriggerDisappear();
        rigidbody_.useGravity = true;
        transform.parent = null;
    }

    public void TriggerDisappear() {
        animator_.SetTrigger("Disappear");
    }

    public void DestroyArrow() {
        arrows_.Remove(this);
        Destroy(gameObject);
    }

    private void OnCollision(Collider other) {
        if (state_ != ArrowState.Flying) {
            return;
        }
        transform.position = preHitPosition_;
        transform.rotation = preHitRotation_;
        if (other.gameObject.layer == movableObjectLayer_) {
            transform.SetParent(other.transform);
        }

        velocity_ = transform.forward * Mathf.Clamp(rigidbody_.velocity.magnitude, minSpeed_, maxSpeed_);
        rigidbody_.isKinematic = true;
        rigidbody_.freezeRotation = true;
        state_ = ArrowState.Hit;

        ScriptedEventController target = other.GetComponent<ScriptedEventController>();
        if (target != null) {
            target.Hit();
            TriggerDisappear();
        } else if (other.gameObject.layer == LayerMask.NameToLayer("EnemyAdditional")) {
            ScriptedEventController knight = other.GetComponentInParent<ScriptedEventController>();
            if (knight != null) {
                knight.Hit();
                TriggerDisappear();
            }
        }
    }
}
