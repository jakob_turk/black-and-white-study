﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Events;

public class TargetController : ScriptedEventController
{
    [SerializeField] private PlayableDirector spawn_timeline_;
    [SerializeField] private PlayableDirector hit_timeline_;
    [SerializeField] private ParticleSystem particles_;
    [SerializeField] private Collider collider_;

    [Header("Dynamic params")]
    [SerializeField] private bool react_to_distance_ = true;
    [SerializeField] private float far_distance_;
    [SerializeField] private float close_distance_;
    [SerializeField] private float far_rotation_;
    [SerializeField] private float close_rotation_;
    [SerializeField] private float far_rate_;
    [SerializeField] private float close_rate_;

    [HideInInspector] public bool hit_ = false;

    private ParticleSystem.RotationOverLifetimeModule ps_rotation_;
    private ParticleSystem.EmissionModule ps_emission_;

    protected override void Awake() {
        base.Awake();
        ps_rotation_ = particles_.rotationOverLifetime;
        ps_emission_ = particles_.emission;
    }

    void Update() {
        if (!react_to_distance_) {
            return;
        }
        Vector3 vec_player_cam = CharacterController.GetCamera().position - transform.position;
        transform.forward = vec_player_cam;

        float normalised_distance = Mathf.Clamp01((vec_player_cam.magnitude - close_distance_)/(far_distance_ - close_distance_));
        float new_rotation = normalised_distance * (far_rotation_ - close_rotation_) + close_rotation_;

        ParticleSystem.MinMaxCurve new_rotation_curve = ps_rotation_.x;
        new_rotation_curve.constant = new_rotation;
        ps_rotation_.x = new_rotation_curve;
        new_rotation_curve = ps_rotation_.y;
        new_rotation_curve.constant = new_rotation;
        ps_rotation_.y = new_rotation_curve;
        new_rotation_curve = ps_rotation_.z;
        new_rotation_curve.constant = new_rotation;
        ps_rotation_.z = new_rotation_curve;

        ParticleSystem.MinMaxCurve new_emission_curve = ps_emission_.rateOverTime;
        new_emission_curve.constant = normalised_distance * (far_rate_ - close_rate_) + close_rate_;
        ps_emission_.rateOverTime = new_emission_curve;
    }

    public void EnableCollider() {
        collider_.enabled = true;
    }

    public void Spawn(float delay) {
        Invoke("PlaySpawn", delay);
    }

    private void PlaySpawn() {
        spawn_timeline_.Play();
    }

    public override void Hit() {
        if (!hit_) {
            hit_ = true;

            TryTriggerEvent();

            collider_.enabled = false;
            hit_timeline_.Play();
            particles_.Stop();
            ParticleSystem.MainModule main = particles_.main;
            main.simulationSpeed = 2f;
        }
    }

    public void DestroyTarget() {
        Destroy(gameObject);
    }
}
