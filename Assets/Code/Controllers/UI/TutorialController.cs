﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRuby.Tween;

public enum TutorialStep { None, Move, Shoot, Sprint, PickUpTorch, SwapForBow, SwapBack, LightTheFire, End }

public class TutorialController : MonoBehaviour
{
    [SerializeField] private List<TutorialStepData> tutorial_datas_list_;
    private Dictionary<TutorialStep, TutorialStepData> tutorial_datas_ = new Dictionary<TutorialStep, TutorialStepData>();

    private TutorialStep current_step_ = TutorialStep.None;
    private int current_msg_length_ = 0;
    bool use_bot_txt_ = false;

    [SerializeField] private Text txt_tutorial_top_;
    [SerializeField] private Text txt_tutorial_bot_;

    private void Awake() {
        foreach (TutorialStepData data in tutorial_datas_list_) {
            tutorial_datas_.Add(data.step, data);
        }
    }

    public void TriggerStep(TutorialStep step) {
        use_bot_txt_ = false;
        float close_timer = -1f;
        switch (step) {
        case TutorialStep.Move:
            if (AnalyticsManager.GetMoveUsages() > 0) {
                return;
            }
            break;
        case TutorialStep.Shoot:
            if (AnalyticsManager.GetShootUsages() > 0) {
                return;
            }
            break;
        case TutorialStep.Sprint:
            if (AnalyticsManager.GetSprintUsages() > 1) {
                return;
            }
            break;
        case TutorialStep.PickUpTorch:
            if (CharacterController.GetEquipmentController().HasTorchRegistered()) {
                return;
            }
            break;
        case TutorialStep.SwapForBow:
            if (CharacterController.GetEquipmentController().BowEquipped) {
                close_timer = 2f;
            }
            break;
        case TutorialStep.SwapBack:
            if (CharacterController.GetEquipmentController().TorchEquipped) {
                return;
            }
            close_timer = 3f;
            break;
        case TutorialStep.LightTheFire:
            if (!CharacterController.GetEquipmentController().HasTorchRegistered()) {
                return;
            }
            break;
        case TutorialStep.End:
            use_bot_txt_ = true;
            break;
        case TutorialStep.None:
        default:
            return;
        }

        CancelInvoke("ClearMsg");
        CancelInvoke("TypeNextLetter");
        current_step_ = step;
        current_msg_length_ = 1;

        Invoke("TypeNextLetter", 0f);

        txt_tutorial_top_.gameObject.SetActive(!use_bot_txt_);
        txt_tutorial_bot_.gameObject.SetActive(use_bot_txt_);

        if (close_timer > 0f) {
            Invoke("ClearMsg", close_timer);
        }
    }

    private void TypeNextLetter() {
        if (use_bot_txt_) {
            txt_tutorial_bot_.text = tutorial_datas_[current_step_].msg.Substring(0, current_msg_length_);
        } else {
            txt_tutorial_top_.text = tutorial_datas_[current_step_].msg.Substring(0, current_msg_length_);
        }

        if (current_msg_length_ < tutorial_datas_[current_step_].msg.Length) {
            current_msg_length_++;
            while (current_msg_length_ < tutorial_datas_[current_step_].msg.Length &&
                   (tutorial_datas_[current_step_].msg[current_msg_length_] == ' ' ||
                    tutorial_datas_[current_step_].msg[current_msg_length_] == ',')) {
                current_msg_length_++;
            }

            Invoke("TypeNextLetter", 0.07f);
        }
    }

    private void ClearMsg() {
        CancelInvoke("TypeNextLetter");
        current_step_ = TutorialStep.None;
        txt_tutorial_top_.text = string.Empty;
        txt_tutorial_top_.gameObject.SetActive(false);
        txt_tutorial_bot_.text = string.Empty;
        txt_tutorial_bot_.gameObject.SetActive(false);
    }

    public void ReportStepDone(TutorialStep step) {
        if (step == current_step_) {
            ClearMsg();
        }
    }
}
