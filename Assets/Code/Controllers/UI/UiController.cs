﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRuby.Tween;

public class UiController : MonoBehaviour
{
    [SerializeField] private TutorialController tutorial_;
    [SerializeField] private Animator animator_death_;
    [SerializeField] private Animator animator_credits_;
    [SerializeField] private GameObject txt_credits_close_;
    [SerializeField] private GameObject txt_credits_exit_;
    [SerializeField] private GameObject menu_;
    [SerializeField] private Graphic menu_bg_;
    [SerializeField] private GameObject crosshair_;
    [SerializeField] private Slider mouse_sensitivity_;

    private List<Graphic> menu_graphics_ = new List<Graphic>();
    private bool menu_active_ = false;

    private static UiController uiController_;

    private void Awake() {
        if (uiController_ != null) {
            Destroy(gameObject);
        } else {
            uiController_ = this;
        }
    }

    private void Start() {
        InputManager.Instance.OnEsc += ToggleMenu;

        // Eh, what you gonna do...
        AddGraphicsOnTransform(menu_.transform);

        foreach (Graphic graphic in menu_graphics_) {
            graphic.color = new Color(graphic.color.r, graphic.color.g, graphic.color.b, 0f);
        }
        menu_bg_.color = new Color(menu_bg_.color.r, menu_bg_.color.g, menu_bg_.color.b, 0f);

        mouse_sensitivity_.value = InputManager.Instance.GetMouseSensitivityNormalized();
        mouse_sensitivity_.onValueChanged.AddListener(SetMouseSensitivity);
    }

    private void AddGraphicsOnTransform(Transform trans) {
        Graphic[] grs = trans.GetComponents<Graphic>();
        if (grs.Length != 0) {
            foreach (Graphic gr in grs) {
                menu_graphics_.Add(gr);
            }
        }
        foreach (Transform tr in trans) {
            AddGraphicsOnTransform(tr);
        }
    }

    private void OnDestroy() {
        InputManager.Instance.OnEsc -= ToggleMenu;
    }

    public static void TriggerDeathUi() {
        uiController_.animator_death_.SetTrigger("Die");
    }

    private void ToggleMenu() {
        if (!menu_active_) {
            ShowMenu();
        } else {
            HideMenu();
        }
    }

    public void ShowMenu() {
        CharacterController.EnterMenu();
        menu_.SetActive(true);
        gameObject.Tween("menu_gradient", menu_graphics_[0].color.a, 1f, 0.7f, TweenScaleFunctions.QuadraticEaseInOut, 
           (p) => {
               foreach (Graphic graphic in menu_graphics_) {
                   graphic.color = new Color(graphic.color.r, graphic.color.g, graphic.color.b, p.CurrentValue);
               }
               menu_bg_.color = new Color(menu_bg_.color.r, menu_bg_.color.g, menu_bg_.color.b, 0.8f * p.CurrentValue);
           });
        menu_active_ = true;
    }

    public void HideMenu() {
        CharacterController.ExitMenu();
        gameObject.Tween("menu_gradient", menu_graphics_[0].color.a, 0f, 0.4f, TweenScaleFunctions.QuadraticEaseInOut,
           (p) => {
               foreach (Graphic graphic in menu_graphics_) {
                   graphic.color = new Color(graphic.color.r, graphic.color.g, graphic.color.b, p.CurrentValue);
               }
               menu_bg_.color = new Color(menu_bg_.color.r, menu_bg_.color.g, menu_bg_.color.b, 0.8f * p.CurrentValue);
           },
           (p) => { menu_.SetActive(false); });
        menu_active_ = false;
    }

    public void ShowCreditsNonStatic() {
        ShowCredits(false);
    }

    public static void ShowCredits(bool end) {
        uiController_.txt_credits_close_.SetActive(!end);
        uiController_.txt_credits_exit_.SetActive(end);

        uiController_.animator_credits_.SetTrigger("Show");

        if (!end) {
            uiController_.animator_credits_.speed = 3f;
            InputManager.Instance.OnAnyKey += CloseCredits;
        }
    }

    public static void CloseCredits() {
        InputManager.Instance.OnAnyKey -= CloseCredits;
        
        uiController_.animator_credits_.SetTrigger("Hide");
        uiController_.animator_credits_.speed = 1f;
    }

    public void ExitGame() {
        GameManager.ExitGame();
    }

    public void SetMouseSensitivity(float value) {
        InputManager.Instance.SetMouseSensitivity(value);
    }

    public void ToggleCrosshair(bool active) {
        crosshair_.SetActive(active);
    }

    public static bool IsCrosshairActive() {
        return uiController_.crosshair_.activeSelf;
    }

    public static TutorialController GetTutorialController() {
        return uiController_.tutorial_;
    }
}
