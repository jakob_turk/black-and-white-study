﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class BlinkingTextController : MonoBehaviour
{
    [SerializeField] private Text text_;
    [SerializeField] private string string_;
    [SerializeField] private string string_blink_ = "_";

    private bool blink_ = false;

    private void OnEnable() {
        InvokeRepeating("Blink", 0f, 0.8f);
    }

    private void OnDisable() {
        CancelInvoke("Blink");
    }

    private void Blink() {
        text_.text = string_ + (blink_ ? string_blink_ : "");
        blink_ = !blink_;
    }
}
