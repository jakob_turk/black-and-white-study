﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckpointController : MonoBehaviour
{
    [SerializeField] public int checkpoint_number_ = 0;

    [SerializeField] protected UnityEvent on_enter_events_;
    [SerializeField] protected UnityEvent respawn_events_;
    [SerializeField] protected UnityEvent debug_events_;

    private void OnTriggerEnter(Collider other) {
        GameManager.SetCurrentCheckpoint(this);
        TriggerOnEnterEvents();
    }

    public void TriggerOnEnterEvents() {
        on_enter_events_?.Invoke();
    }

    public void TriggerRespawnEvents() {
        respawn_events_?.Invoke();
    }

    public void TriggerDebugEvents() {
        debug_events_?.Invoke();
    }
}
