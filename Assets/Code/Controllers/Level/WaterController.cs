﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;

public class WaterController : AbyssController
{
    [SerializeField] private List<float> heights_;
    [SerializeField] private List<float> durations_;

    private int reached_level_ = 0;
    private bool raising_ = false;

    private float pos_x_;
    private float pos_z_;

    private void Awake() {
        pos_x_ = transform.position.x;
        pos_z_ = transform.position.z;
    }

    public void ReachWaterRaiseStart(int level) {
        reached_level_ = level;
        if (!raising_) {
            RaiseToWaterLevel(level);
        }
    }

    private void RaiseToWaterLevel(int level) {
        raising_ = true;
        gameObject.Tween("water_level", transform.position.y, heights_[level], durations_[level], TweenScaleFunctions.QuadraticEaseInOut,
            (p) => {
                transform.position = new Vector3(pos_x_, p.CurrentValue, pos_z_);
            },
            (p) => {
                raising_ = false;
                if (reached_level_ > level) {
                    RaiseToWaterLevel(level + 1);
                }
            });
    }

    public void ResetWaterLevel() {
        reached_level_ = 0;
        raising_ = false;

        gameObject.Tween("water_level", 0f, 0f, 0.1f, TweenScaleFunctions.Linear, (p) => { });
        transform.position = new Vector3(pos_x_, heights_[0], pos_z_);
    }

    protected override void OnDeath() {
        durations_[2] += 2f;
    }
}
