﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbyssController : MonoBehaviour
{
    [SerializeField] private bool free_fall_ = true;

    private void OnTriggerEnter(Collider other) {
        CharacterController.Die(free_fall_);
        OnDeath();
    }

    protected virtual void OnDeath() { }
}
