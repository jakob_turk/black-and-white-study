﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerController : ScriptedEventController
{
    [SerializeField] private float delay_ = 0f;

    private void OnTriggerEnter(Collider other) {
        Invoke("InvokeEvent", delay_);
    }

    private void InvokeEvent() {
        TryTriggerEvent();
    }
}
