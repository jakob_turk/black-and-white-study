﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnController : MonoBehaviour
{
    [SerializeField] private int sustained_enemy_count_;
    [SerializeField] private int enemy_count_;
    [SerializeField] private float spawn_period_;
    [SerializeField] private float radius_;

    [SerializeField] private KnightController knightPrefab_;

    [SerializeField] private float detection_radius_factor_ = 1f;
    [SerializeField] private bool force_chasing_ = false;
    [SerializeField] private bool spawning_ = false;

    private bool currently_spawning_ = false;
    private int spawned_count_ = 0;
    private float timer_ = -1f;
    private List<KnightController> spawned_enemies_ = new List<KnightController>();

    private void Awake() {
        currently_spawning_ = spawning_;
        if (currently_spawning_) {
            Spawn();
        }
    }

    private void Update() {
        if (timer_ >= 0f) {
            timer_ += Time.deltaTime;
            if (timer_ > spawn_period_) {
                timer_ = -1f;
                if (currently_spawning_ && (spawned_enemies_.Count < sustained_enemy_count_ || spawned_count_ < enemy_count_)) {
                    Spawn();
                }
            }
        }
    }

    private void Spawn() {
        spawned_count_++;
        Vector2 random_pos = Random.insideUnitCircle;
        KnightController new_enemy = Instantiate(knightPrefab_, transform.position + new Vector3(random_pos.x, 0f, random_pos.y) * radius_, transform.rotation);
        new_enemy.Init(this, detection_radius_factor_);
        if (force_chasing_) {
            new_enemy.ForceChase();
        }

        spawned_enemies_.Add(new_enemy);
        if (currently_spawning_ && (spawned_enemies_.Count < sustained_enemy_count_ || spawned_count_ < enemy_count_)) {
            timer_ = 0f;
        }
    }

    public void ReportDeath(KnightController knight) {
        spawned_enemies_.Remove(knight);
        if (currently_spawning_ && spawned_enemies_.Count < sustained_enemy_count_) {
            timer_ = 0f;
        }
    }

    public void StartSpawnign() {
        currently_spawning_ = true;
        if (spawned_enemies_.Count < sustained_enemy_count_ || spawned_count_ < enemy_count_) {
            Spawn();
        }
    }

    public void StopSpawning() {
        currently_spawning_ = false;
    }

    public void StopSpawningHard() {
        Destroy(gameObject);
    }

    public void StopSpawningAndCleanup() {
        currently_spawning_ = false;
        foreach (KnightController enemy in spawned_enemies_) {
            Destroy(enemy.gameObject);
        }
        spawned_enemies_.Clear();
    }

    public void ResetSpawn() {
        StopSpawningAndCleanup();

        spawned_count_ = 0;
        currently_spawning_ = spawning_;
        if (currently_spawning_ && (spawned_enemies_.Count < sustained_enemy_count_ || spawned_count_ < enemy_count_)) {
            timer_ = 0f;
        }
    }

    public void ResetSpawnAndStartSpawning() {
        ResetSpawn();
        if (!currently_spawning_) {
            StartSpawnign();
        }
    }

    public void ForceChaseSpawned() {
        foreach (KnightController enemy in spawned_enemies_) {
            enemy.ForceChase();
        }
    }
}
