﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScriptedEventController : MonoBehaviour
{
    [SerializeField] protected int event_group_id_ = -1;
    [SerializeField] protected UnityEvent event_;

    private bool triggered_ = false;

    private static Dictionary<int, List<ScriptedEventController>> event_script_groups_ = new Dictionary<int, List<ScriptedEventController>>();

    protected virtual void Awake() {
        if (event_group_id_ > -1) {
            // Debug.Log(event_group_id_);
        }
        if (event_group_id_ >= 0) {
            if (!event_script_groups_.ContainsKey(event_group_id_)) {
                event_script_groups_.Add(event_group_id_, new List<ScriptedEventController>());
            }
            event_script_groups_[event_group_id_].Add(this);
        }
    }

    public bool TryTriggerEvent() {
        triggered_ = true;

        if (event_group_id_ >= 0) {
            foreach (ScriptedEventController event_script in event_script_groups_[event_group_id_]) {
                if (!event_script.triggered_) {
                    return false;
                }
            }
            event_?.Invoke();
        } else {
            event_?.Invoke();
        }
        return true;
    }

    public void ForceEvent(float delay = 0f) {
        Invoke("ForceEventCall", delay);
    }

    private void ForceEventCall() {
        triggered_ = true;
        event_?.Invoke();
    }

    public void ResetEvent() {
        triggered_ = false;
    }

    public void SetEvent(UnityEvent new_event) {
        event_ = new_event;
    }

    public virtual void Hit() { }
}
