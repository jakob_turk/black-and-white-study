﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropHatchController : MonoBehaviour
{
    [SerializeField] private Transform hatch_;

    public void PositionHatch() {
        Vector3 player_pos = CharacterController.GetEquipmentController().GetSpine().position;
        hatch_.position = new Vector3(player_pos.x, hatch_.position.y, player_pos.z);
    }
}
