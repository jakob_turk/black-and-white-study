﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowCatcher : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        ArrowController arrow = other.GetComponent<ArrowController>();
        if (arrow != null) {
            arrow.DestroyArrow();
        }
    }
}
