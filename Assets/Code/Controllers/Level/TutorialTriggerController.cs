﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTriggerController : MonoBehaviour
{
    [SerializeField] private TutorialStep step_;
    [SerializeField] private float delay_ = 0f;
    private bool reached_ = false;

    private void OnTriggerEnter(Collider other) {
        if (!reached_) {
            Invoke("TriggerTutorialStep", delay_);
            reached_ = true;
        }
    }

    private void TriggerTutorialStep() {
        UiController.GetTutorialController().TriggerStep(step_);
    }
}
