﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

struct Boid
{
    public Vector3 position;
    public Vector3 velocity;
    public Vector3 direction;
    public Vector3 up;
    public Matrix4x4 rotation_matrix;
    public float color;

    public Boid(Vector3 pos, Vector3 vel, Vector3 ups, Matrix4x4 mat, float col) {
        position = pos;
        velocity = vel;
        direction = vel.normalized;
        up = ups;
        rotation_matrix = mat;
        color = col;
    }
};

public class BoidController : MonoBehaviour
{
    [SerializeField] private ComputeShader compute_shader_;
    [SerializeField] private Material material_;
    [SerializeField] private Mesh mesh_;

    [SerializeField] private int n_boids_;
    [SerializeField] private float view_radius_;
    [SerializeField] private float avoid_radius_;
    [SerializeField] private float max_speed_;
    [SerializeField] private float far_wall_;
    [SerializeField] private float align_weight_;
    [SerializeField] private float cohesion_weight_;
    [SerializeField] private float separate_weight_;

    private ComputeBuffer boids_buffer_;
    private Bounds bounds_;
    private int n_groups_;
    private static readonly int boids_id_ = Shader.PropertyToID("_boids");
    private static readonly int n_boids_id_ = Shader.PropertyToID("_numBoids");
    private static readonly int view_radius_id_ = Shader.PropertyToID("_viewRadius");
    private static readonly int avoid_radius_id_ = Shader.PropertyToID("_avoidRadius");
    private static readonly int max_speed_id_ = Shader.PropertyToID("_maxSpeed");
    private static readonly int far_wall_id_ = Shader.PropertyToID("_farWall");
    private static readonly int align_weight_id_ = Shader.PropertyToID("_alignWeight");
    private static readonly int cohesion_weight_id_ = Shader.PropertyToID("_cohesionWeight");
    private static readonly int separate_weight_id_ = Shader.PropertyToID("_seperateWeight");
    private static readonly int time_step_id_ = Shader.PropertyToID("_timeStep");
    private static readonly int world_position_id_ = Shader.PropertyToID("_worldPosition");

    private void OnEnable() {
        OnDisable();

        n_groups_ = Mathf.CeilToInt(n_boids_ / 512f);
        Boid[] boids = SetUpBoidData(n_boids_);
        boids_buffer_ = new ComputeBuffer(n_boids_, sizeof(float) * 29);
        boids_buffer_.SetData(boids);

        bounds_ = new Bounds(transform.position, Vector3.one * 20f);
        material_.SetVector(world_position_id_, transform.position * 2f);

        material_.SetBuffer(boids_id_, boids_buffer_);
        material_.SetFloat(far_wall_id_, far_wall_);
        compute_shader_.SetBuffer(0, boids_id_, boids_buffer_);
        compute_shader_.SetInt(n_boids_id_, n_boids_);
        compute_shader_.SetFloat(view_radius_id_, view_radius_);
        compute_shader_.SetFloat(avoid_radius_id_, avoid_radius_);
        compute_shader_.SetFloat(max_speed_id_, max_speed_);
        compute_shader_.SetFloat(far_wall_id_, far_wall_);
        compute_shader_.SetFloat(align_weight_id_, align_weight_);
        compute_shader_.SetFloat(cohesion_weight_id_, cohesion_weight_);
        compute_shader_.SetFloat(separate_weight_id_, separate_weight_);
    }

    private void OnDisable() {
        if (boids_buffer_ != null) {
            boids_buffer_.Release();
            boids_buffer_ = null;
        }
    }

    private void Update() {
        if (boids_buffer_ == null) {
            return;
        }

        UpdateBoids();
        DrawBoids();
    }

    private void UpdateBoids() {
        compute_shader_.SetFloat(time_step_id_, Time.deltaTime);
        compute_shader_.Dispatch(0, n_groups_, 1, 1);
    }

    private void DrawBoids() {
        Graphics.DrawMeshInstancedProcedural(mesh_, 0, material_, bounds_, boids_buffer_.count);
    }

    private Boid[] SetUpBoidData(int count) {
        Boid[] boids = new Boid[count];
        for (int i = 0; i < count; i++) {
            float theta = Random.Range(45f, 160f);
            float phi = Random.Range(0f, 360f);
            float r = Random.Range(far_wall_ / 4f, far_wall_);
            float sin_theta = Mathf.Sin(theta);
            Vector3 pos = new Vector3(sin_theta * Mathf.Cos(phi), sin_theta * Mathf.Sin(phi), Mathf.Cos(theta)) * r;
            Vector2 pos2d = new Vector2(pos.x, pos.z);
            pos2d += pos2d.normalized * (far_wall_ / 2f);
            pos = new Vector3(pos2d.x, pos.y, pos2d.y);
            boids[i] = new Boid(pos, Random.onUnitSphere * 5f, Random.onUnitSphere, Matrix4x4.identity, Random.value < 0.5f ? 0f : 1f);
        }
        return boids;
    }
}
