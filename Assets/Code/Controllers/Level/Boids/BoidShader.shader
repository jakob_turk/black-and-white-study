﻿Shader "Unlit/BoidShader"
{
    Properties
    {
    }
    SubShader
    {
        Tags { "RenderType"="OpaqueBoid" }

        Pass
        {
			Cull Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile_fwdbase
			#pragma target 4.5
            #include "UnityCG.cginc"

			struct Boid {
				float3 position;
				float3 velocity;
				float3 direction;
				float3 up;
				float4x4 rotation_matrix;
				float color;
			};

#if SHADER_TARGET >= 45
			StructuredBuffer<Boid> _boids;
#endif
			float4 _worldPosition;
			float _farWall;

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
				float4 pos : SV_POSITION;
				float4 loc_pos : TEXCOORD0;
				float cam_dist : TEXCOORD1;
				float col : TEXCOORD2;
            };

            v2f vert (appdata v, uint instanceID : SV_InstanceID)
            {
#if SHADER_TARGET >= 45
				Boid boid = _boids[instanceID];
#else
				Boid boid;
#endif
				float scale = 100.f;
				float4x4 scale_matrix = float4x4(
					float4(scale, 0.f, 0.f, 0.f),
					float4(0.f, scale, 0.f, 0.f),
					float4(0.f, 0.f, scale, 0.f),
					float4(0.f, 0.f, 0.f, 1.f)
				);

				v2f o;
				o.loc_pos = mul(mul(boid.rotation_matrix, scale_matrix), float4(v.vertex.xyz, 1.f)) + float4(boid.position.xyz, 1.f);
				o.pos = mul(UNITY_MATRIX_VP, o.loc_pos + _worldPosition);
				o.cam_dist = length(WorldSpaceViewDir(o.loc_pos + _worldPosition));
				o.col = boid.color;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				clip(dot(i.loc_pos, i.loc_pos) - 20.f);
				clip(i.cam_dist - 20.f);
                return fixed4(i.col, i.col, i.col, 1.f);
            }
            ENDCG
        }
    }
}
