﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalController : MonoBehaviour
{
    [SerializeField] private PortalController connectedPortal_;

    [SerializeField] public MeshRenderer portalScreen_;
    [SerializeField] public Camera portalCamera_;

    private Camera playerCamera_;
    private RenderTexture portalViewTexture_;

    public static List<PortalController> allPortals_ = new List<PortalController>();

    private void Awake() {
        allPortals_.Add(this);
    }

    private void SetUpPortal() {
        if (portalViewTexture_ != null && portalViewTexture_.width == Screen.width && portalViewTexture_.height == Screen.height) {
            return;
        }
        if (portalViewTexture_ != null) {
            portalViewTexture_.Release();
        }
        portalViewTexture_ = new RenderTexture(Screen.width, Screen.height, 0);
        portalCamera_.targetTexture = portalViewTexture_;
        portalScreen_.material.SetTexture("_MainTex", portalViewTexture_);
        portalScreen_.material.SetFloat("_PortalTextureOffset", -0.5f * (1 - (float)portalViewTexture_.width / (float)Screen.width));
        portalScreen_.material.SetFloat("_PortalTextureScaleRatio", (float)Screen.width / (float)portalViewTexture_.width);
    }

    static bool VisibleFromCamera(Renderer renderer, Camera camera) {
        Plane[] frustomPlanes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB(frustomPlanes, renderer.bounds);
    }

    public void Render() {
        if (connectedPortal_ == null) {
            Debug.Log("Lonely portal!");
            return;
        }
        if (playerCamera_ == null) {
            playerCamera_ = Camera.main;
            portalCamera_.enabled = false;
        }

        if (!VisibleFromCamera(portalScreen_, playerCamera_)) {
            return;
        }

        portalScreen_.enabled = false;

        SetUpPortal();

        // Matrix4x4 m = transform.localToWorldMatrix * connectedPortal_.transform.worldToLocalMatrix * playerCamera_.transform.localToWorldMatrix;
        // portalCamera_.transform.SetPositionAndRotation(m.GetColumn(3), m.rotation);
        portalCamera_.transform.position = connectedPortal_.transform.position + playerCamera_.transform.position - transform.position;
        portalCamera_.transform.rotation = playerCamera_.transform.rotation;
        portalCamera_.Render();

        portalScreen_.enabled = true;
    }

    private void OnDestroy() {
        allPortals_.Remove(this);
    }
}
