﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TargetSpawner : MonoBehaviour
{
    [SerializeField] private TargetController target_prefab_;
    [SerializeField] private UnityEvent target_events_;

    private TargetController target_;

    public void Spawn() {
        ResetSpawn();
        target_ = Instantiate(target_prefab_, transform);
        target_.SetEvent(target_events_);
        target_.Spawn(0f);
    }

    public void ResetSpawn() {
        if (target_ != null) {
            Destroy(target_.gameObject);
        }
    }
}
