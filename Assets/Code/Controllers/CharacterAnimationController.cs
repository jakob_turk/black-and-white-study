﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;

public class CharacterAnimationController : MonoBehaviour
{
    [SerializeField] private Animator animator_;
    [SerializeField] private Transform look_at_transform_;
    private Transform look_at_;

    [Header("Idle weights")]
    [SerializeField] private float[] idle_values_;
    [SerializeField] private float[] idle_weights_;
    [SerializeField] private float idle_look_at_threshold_ = 0f;
    [SerializeField] private float idle_change_speed_ = 0.1f;
    [SerializeField] private float look_at_weight_change_speed_ = 1f;

    private float weights_sum_ = 0f;

    [HideInInspector] public float aim_weight_ = 0f;
    public bool prevent_look_at_ = false;

    private float current_idle_type_ = 0f;
    private float ik_look_at_weight_ = 1f;

    private string tween_id_;

    private void Awake() {
        if (idle_values_.Length != idle_weights_.Length) {
            Debug.LogError("Idle values and weights do not match!");
        }
        weights_sum_ = ArraySum(idle_weights_, idle_weights_.Length);

        look_at_ = look_at_transform_;

        tween_id_ = "char_anim_idle_" + gameObject.GetInstanceID().ToString();
    }

    public void SetIdleAnimation() {
        float old_idle_type = current_idle_type_;
        float next_rand = Random.Range(0, weights_sum_);
        for (int i = 0; i < idle_values_.Length; i++) {
            float cumm_weight = ArraySum(idle_weights_, i + 1);
            if (next_rand < cumm_weight) {
                current_idle_type_ = idle_values_[i];
                break;
            }
        }

        gameObject.Tween(tween_id_, old_idle_type, current_idle_type_, idle_change_speed_,
            TweenScaleFunctions.QuadraticEaseInOut, (p) => { if (animator_ != null) { animator_.SetFloat("IdleType", p.CurrentValue); } });
    }

    private void OnAnimatorIK(int layerIndex) {
        float target_weight = prevent_look_at_ || current_idle_type_ > idle_look_at_threshold_ ? 0f : 1f;
        if (ik_look_at_weight_ < target_weight) {
            ik_look_at_weight_ += Time.deltaTime * look_at_weight_change_speed_;
            if (ik_look_at_weight_ > target_weight)
                ik_look_at_weight_ = target_weight;
        } else if (ik_look_at_weight_ > target_weight) {
            ik_look_at_weight_ -= Time.deltaTime * look_at_weight_change_speed_;
            if (ik_look_at_weight_ < target_weight)
                ik_look_at_weight_ = target_weight;
        }
        
        animator_.SetLookAtWeight(aim_weight_ > 0 ? aim_weight_ : ik_look_at_weight_, aim_weight_);
        if (look_at_ != null) {
            animator_.SetLookAtPosition(look_at_.position);
        }
    }

    public void SetLookAt(Transform transform) {
        look_at_ = transform;
    }

    private float ArraySum(float[] array, int n) {
        float sum = 0f;
        for (int i = 0; i < n; i++) {
            sum += array[i];
        }
        return sum;
    }
}
