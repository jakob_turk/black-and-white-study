﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowAnimationEvents : MonoBehaviour
{
    public delegate void OnDestroyArrow();
    public OnDestroyArrow onDestroyArrow;

    public void DestroyArrow() {
        onDestroyArrow?.Invoke();
    }
}
