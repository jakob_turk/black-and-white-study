﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawArrowAnimationController : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        // This cannot be an event, as layer is usually inactive on start and animation does not really play
        CharacterController.SetShooting();
    }
}
