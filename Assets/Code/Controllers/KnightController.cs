﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;

public enum EnemyState { Idle, Aware, Chasing, Attacking, Dead, Won };

public class KnightController : ScriptedEventController
{
    [SerializeField] private Animator animator_;
    [SerializeField] private CharacterAnimationController animation_controller_;
    [SerializeField] private SkinnedMeshRenderer mesh_;
    [SerializeField] private SkinnedMeshRenderer mesh_helmet_;
    [SerializeField] private MeshRenderer mesh_sword_;
    [SerializeField] private ParticleSystem ps_;
    [SerializeField] private ParticleSystem ps_death_burst_;
    [SerializeField] private ParticleSystem ps_light_burst_;
    [SerializeField] private TriggerDelegateController aware_trigger_;
    [SerializeField] private TriggerDelegateController chase_trigger_;
    [SerializeField] private TriggerDelegateController attack_trigger_;
    [SerializeField] private SphereCollider aware_trigger_collider_;
    [SerializeField] private SphereCollider chase_trigger_collider_;
    [SerializeField] private SphereCollider attack_trigger_collider_;
    [SerializeField] private Collider[] attack_trigers_;
    [SerializeField] private Material visible_material_;

    private EnemySpawnController spawn_ = null;
    private string tween_id_;
    private bool force_chase_;
    private Vector3 start_position_;

    public EnemyState State { get; private set; }

    protected override void Awake() {
        base.Awake();

        start_position_ = transform.position;

        tween_id_ = "enemy_rotation" + gameObject.GetInstanceID().ToString();
    }

    void Start() {
        aware_trigger_.onCollision += OnAwareTrigger;
        aware_trigger_.onExitTrigger += OnExitAwareTrigger;
        chase_trigger_.onCollision += OnChaseTrigger;
        chase_trigger_.onExitTrigger += OnExitChaseTrigger;
        attack_trigger_.onCollision += (collider) => OnAttackTrigger(collider, false);
    }

    private void OnDestroy() {
        aware_trigger_.onCollision -= OnAwareTrigger;
        aware_trigger_.onExitTrigger -= OnExitAwareTrigger;
        chase_trigger_.onCollision -= OnChaseTrigger;
        chase_trigger_.onExitTrigger -= OnExitChaseTrigger;
        attack_trigger_.onCollision -= (collider) => OnAttackTrigger(collider, false);
    }

    void Update() {
        if (State == EnemyState.Chasing) {
            Vector3 player_pos = CharacterController.GetEquipmentController().GetSpine().position;
            transform.forward = new Vector3(player_pos.x, 0f, player_pos.z) - new Vector3(transform.position.x, 0f, transform.position.z);
        }
    }

    private void OnAwareTrigger(Collider other) {
        if (State == EnemyState.Dead) {
            return;
        }
        if (State != EnemyState.Chasing) {
            State = EnemyState.Aware;
        }
        animation_controller_.SetLookAt(other.transform);
        animation_controller_.prevent_look_at_ = false;
    }
    private void OnExitAwareTrigger(Collider other) {
        if (force_chase_) {
            return;
        }
        State = EnemyState.Idle;
        animation_controller_.SetLookAt(null);
        animation_controller_.prevent_look_at_ = true;
    }

    private void OnChaseTrigger(Collider other) {
        if (State == EnemyState.Chasing || State == EnemyState.Dead) {
            return;
        }

        animator_.SetTrigger("Chase");

        Vector3 player_pos = CharacterController.GetEquipmentController().GetSpine().position;
        Vector3 look_direction_xz = new Vector3(player_pos.x, 0f, player_pos.z) - new Vector3(transform.position.x, 0f, transform.position.z);
        gameObject.Tween(tween_id_, transform.rotation, Quaternion.LookRotation(look_direction_xz, Vector3.up),
            1f, TweenScaleFunctions.QuadraticEaseInOut, (p) => { if (this != null && transform != null) { transform.rotation = p.CurrentValue; } },
            (p) => { if (this != null && transform != null) { State = EnemyState.Chasing; } });
    }
    private void OnExitChaseTrigger(Collider other) {
        if (force_chase_) {
            return;
        }
        State = EnemyState.Aware;
        animator_.SetTrigger("Idle");
        gameObject.Tween(tween_id_, transform.rotation, transform.rotation, 0.1f, TweenScaleFunctions.QuadraticEaseInOut,
            (p) => { if (this != null && transform != null) { transform.rotation = p.CurrentValue; } });
    }

    private void OnAttackTrigger(Collider other, bool close) {
        if (State == EnemyState.Dead) {
            return;
        }
        State = EnemyState.Attacking;
        
        bool low_ceiling = Physics.Raycast(transform.position, transform.up, 2.5f, ~LayerMask.GetMask("Arrow", "CharacterTrigger", "EnemyAdditional", "Light"));
        animator_.SetFloat("AttackType", low_ceiling || close ? 1f : 0f);
        if (low_ceiling) {
            Invoke("TriggerAttack", 0.4f);
        } else {
            animator_.SetTrigger("Attack");
        }
    }

    private void TriggerAttack() {
        animator_.SetTrigger("Attack");
    }

    public void OnKill() {
        State = EnemyState.Won;
        animator_.SetBool("Win", true);
    }

    public void EndAttack() {
        if (State == EnemyState.Won) {
            return;
        }
        if (Physics.CheckSphere(attack_trigger_.transform.position, attack_trigger_collider_.radius, LayerMask.GetMask("Character"), QueryTriggerInteraction.Collide)) {
            Vector3 player_pos = CharacterController.GetEquipmentController().GetSpine().position;
            Vector3 look_direction_xz = new Vector3(player_pos.x, 0f, player_pos.z) - new Vector3(transform.position.x, 0f, transform.position.z);
            gameObject.Tween(tween_id_, transform.rotation, Quaternion.LookRotation(look_direction_xz, Vector3.up),
                0.5f, TweenScaleFunctions.QuadraticEaseInOut, (p) => { if (this != null && transform != null) { transform.rotation = p.CurrentValue; } }, 
                (p) => { if (this != null && transform != null) { State = EnemyState.Chasing; } });
            OnAttackTrigger(null, true);
        } else {
            OnChaseTrigger(null);
        }
    }

    public override void Hit() {
        animator_.SetTrigger("Die");
        ps_death_burst_.Play();
        Die();
    }

    public void TouchTheLight() {
        animator_.SetTrigger("DieLight");
        ps_light_burst_.Play();
        Die();
    }

    private void Die() {
        State = EnemyState.Dead;
        TryTriggerEvent();
        spawn_?.ReportDeath(this);
        foreach (Collider collider in attack_trigers_) {
            collider.enabled = false;
        }

        ps_.Stop();
        // reset tween
        gameObject.Tween(tween_id_, 0f, 0f, 0.1f, TweenScaleFunctions.Linear, (p) => {});
    }

    public void TurnOnShadows() {
        mesh_.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        mesh_helmet_.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        mesh_sword_.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        mesh_.receiveShadows = true;
        mesh_helmet_.receiveShadows = true;
        mesh_sword_.receiveShadows = true;
    }

    public void TurnOffShadows() {
        mesh_.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        mesh_helmet_.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        mesh_.receiveShadows = false;
        mesh_helmet_.receiveShadows = false;
    }

    public void TurnOffSwordShadow() {
        mesh_sword_.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        mesh_sword_.receiveShadows = false;
    }

    public void SetDead() {
        animator_.SetBool("Dead", true);
    }

    public void DestroyKnight() {
        Destroy(gameObject);
    }

    public void Init(EnemySpawnController spawn, float detection_radius_factor = 1f) {
        spawn_ = spawn;
        aware_trigger_collider_.radius *= detection_radius_factor;
        chase_trigger_collider_.radius *= detection_radius_factor;
        animator_.SetTrigger("Appear");
    }

    public void ResetKnight() {
        if (State == EnemyState.Dead) {
            DestroyKnight();
        }
        animator_.ResetTrigger("Idle");
        animator_.ResetTrigger("Chase");
        animator_.ResetTrigger("Attack");
        OnExitChaseTrigger(null);
        OnExitAwareTrigger(null);

        transform.position = start_position_;
    }

    public void ForceChase() {
        force_chase_ = true;
        OnAwareTrigger(CharacterController.GetCollider());
        OnChaseTrigger(CharacterController.GetCollider());
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.layer == LayerMask.NameToLayer("Light")) {
            TouchTheLight();
        }
    }
}
