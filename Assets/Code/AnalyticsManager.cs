﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnalyticsManager : MonoBehaviour
{
    private int n_usage_move_;
    private int n_usage_shoot_;
    private int n_usage_sprint_;

    private static AnalyticsManager analyticsManager_;

    private void Awake() {
        if (analyticsManager_ != null) {
            Destroy(gameObject);
        } else {
            analyticsManager_ = this;
        }
    }

    public static void ReportMoveUsage(int n) {
        analyticsManager_.n_usage_move_ += n;
    }

    public static void ReportShootUsage() {
        analyticsManager_.n_usage_shoot_++;
    }

    public static void ReportSprintUsage() {
        analyticsManager_.n_usage_sprint_++;
    }

    public static int GetMoveUsages() {
        return analyticsManager_.n_usage_move_;
    }

    public static int GetShootUsages() {
        return analyticsManager_.n_usage_shoot_;
    }

    public static int GetSprintUsages() {
        return analyticsManager_.n_usage_sprint_;
    }
}
