﻿Shader "Unlit/PortalShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_PortalTextureOffset("Portal Texture Offset", Float) = 0.0
		_PortalTextureScaleRatio("Portal Texture Scale Ratio", Float) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Portal" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 screenPos : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _PortalTextureOffset;
            float _PortalTextureScaleRatio;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.screenPos = ComputeScreenPos(o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float2 screenSpaceUVtex = i.screenPos.xy / i.screenPos.w;
				float2 screenSpaceUV = float2(_PortalTextureOffset + _PortalTextureScaleRatio * screenSpaceUVtex.x, screenSpaceUVtex.y);
                return tex2D(_MainTex, screenSpaceUV);;
            }
            ENDCG
        }
    }
}
