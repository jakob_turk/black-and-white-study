﻿Shader "Unlit/NormalsShader"
{
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
				float4 color : COLOR;
				float3 normal : NORMAL;
            };

            struct v2f
            {
				float4 vertex : SV_POSITION;
				fixed4 faceColor : COLOR;
				float3 viewNormal : NORMAL;

            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.viewNormal = COMPUTE_VIEW_NORMAL;
				o.faceColor = (v.color.r) * float4(normalize(o.viewNormal) * 0.5 + 0.5, 0) + (1 - v.color.r) * float4(v.color.g, v.color.g, v.color.g, 0);
				return o;
			}

			float4 frag(v2f i) : SV_Target
			{
				return i.faceColor;
			}
            ENDCG
        }
    }
}
