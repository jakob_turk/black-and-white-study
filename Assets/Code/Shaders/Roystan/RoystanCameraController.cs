﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RoystanCameraController : MonoBehaviour
{
    [SerializeField] private Camera camera_;
    [SerializeField] private Camera normalsCamera_;

    [SerializeField] private Shader replacementShader_;
    [SerializeField] int renderTextureDepth = 24;

    private RenderTexture renderTexture;

    void Start() {
        camera_.depthTextureMode = DepthTextureMode.Depth;
        
        renderTexture = new RenderTexture(camera_.pixelWidth, camera_.pixelHeight, renderTextureDepth, RenderTextureFormat.ARGB32);
        renderTexture.filterMode = FilterMode.Point;

        Shader.SetGlobalTexture("_CameraNormalsTexture", renderTexture);

        normalsCamera_.CopyFrom(camera_);
        normalsCamera_.targetTexture = renderTexture;
        normalsCamera_.SetReplacementShader(replacementShader_, "RenderType");
        normalsCamera_.depth = camera_.depth - 1;
        normalsCamera_.clearFlags = CameraClearFlags.Color;
        normalsCamera_.backgroundColor = Color.black;
    }

    private void OnPreCull() {
        foreach (PortalController portal in PortalController.allPortals_) {
            portal.Render();
        }
    }
}
