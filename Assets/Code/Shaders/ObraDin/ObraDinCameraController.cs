﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ObraDinCameraController : MonoBehaviour
{
    [SerializeField] private Shader outlinesShader_;
    [SerializeField] private Camera camera_;
    [SerializeField] private Camera outlinesCamera_;
    public static RenderTexture OutlinesTexture;

    // Start is called before the first frame update
    void Start() {
        camera_.depthTextureMode = DepthTextureMode.Depth;

        outlinesCamera_.SetReplacementShader(outlinesShader_, "RenderType");
        OutlinesTexture = new RenderTexture(Screen.width, Screen.height, 1, RenderTextureFormat.ARGBHalf);
        outlinesCamera_.targetTexture = OutlinesTexture;
    }

    private void OnPreCull() {
        foreach (PortalController portal in PortalController.allPortals_) {
            portal.Render();
        }
    }
}
