﻿Shader "Custom/NormalsAndObjectsShader"
{
	Properties
	{
		_DissolveTex("Dissolve texture", 2D) = "white" {}
		_DissolveTreshold("Dissolve treshold", Range(0, 1)) = 0.0
		_DissolveEdge("Dissolve edge", Range(0, 1)) = 0.0
	}

    SubShader
    {
		Tags { "RenderType" = "Opaque" }
		Pass
		{
			Tags
			{ "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float3 normal: NORMAL;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				fixed4 faceColor : COLOR;
			};

			v2f vert(appdata v)
			{
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);

				fixed3 faceVal = (1 - 0.5 * v.color.r) * v.color.g * (0.1 * fmod(mul(unity_ObjectToWorld, float4(0, 0, 0, 1)), 10)) +
								 (v.color.r) * 0.25 * (v.normal + 1);
				o.faceColor = fixed4(faceVal, 1);

				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				return i.faceColor;
			}
			ENDCG
		}
    }
	SubShader
	{
		Tags { "RenderType" = "OpaqueColor" }
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);

				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				return fixed4(1.f, 1.f, 1.f, 0.f);
			}
			ENDCG
		}
	}
	SubShader
	{
		Tags {"RenderType" = "Dissolve" }
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float3 normal: NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				fixed4 faceColor : COLOR;
			};

			sampler2D _DissolveTex;
			float4 _DissolveTex_ST;

			v2f vert(appdata v)
			{
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _DissolveTex);

				fixed3 faceVal = (1 - 0.5 * v.color.r) * v.color.g * (0.1 * fmod(mul(unity_ObjectToWorld, float4(0, 0, 0, 1)), 10)) +
									(v.color.r) * 0.25 * (v.normal + 1);
				o.faceColor = fixed4(faceVal, 1);

				return o;
			}

			float _DissolveTreshold;

			fixed4 frag(v2f i) : SV_Target
			{
				clip(tex2D(_DissolveTex, i.uv) - _DissolveTreshold);
				return i.faceColor;
			}
			ENDCG
		}
	}
	SubShader
	{
		Tags { "RenderType" = "OpaqueDisolveScreen" }
		Pass
		{
			Tags { "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float3 normal: NORMAL;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 screen_pos : TEXCOORD1;
				fixed4 faceColor : COLOR;
			};

			v2f vert(appdata v)
			{
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.screen_pos = ComputeScreenPos(v.vertex);

				fixed3 faceVal = (1 - 0.5 * v.color.r) * v.color.g * (0.1 * fmod(mul(unity_ObjectToWorld, float4(0, 0, 0, 1)), 10)) +
									(v.color.r) * 0.25 * (v.normal + 1);
				o.faceColor = fixed4(faceVal, 1);

				return o;
			}
			
			sampler2D _DissolveTex;
			float4 _DissolveTex_ST;
			float _DissolveTreshold;
			float _DissolveEdge;

			fixed4 frag(v2f i) : SV_Target
			{
				clip(tex2D(_DissolveTex, i.screen_pos) - _DissolveTreshold);

				return i.faceColor;
			}
			ENDCG
		}
	}
	SubShader
	{
		Tags {"RenderType" = "BridgeDissolve" }
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float3 normal: NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 loc_pos : TEXCOORD0;
				float2 uv : TEXCOORD1;
				fixed4 faceColor : COLOR;
			};

			sampler2D _DissolveTex;
			float4 _DissolveTex_ST;

			v2f vert(appdata v)
			{
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.loc_pos = v.vertex;
				o.uv = TRANSFORM_TEX(float2(v.uv.x - _Time.x, v.uv.y), _DissolveTex);

				fixed3 faceVal = (1 - 0.5 * v.color.r) * v.color.g * (0.1 * fmod(mul(unity_ObjectToWorld, float4(0, 0, 0, 1)), 10)) +
									(v.color.r) * 0.25 * (v.normal + 1);
				o.faceColor = fixed4(faceVal, 1);

				return o;
			}

			float _DissolveEdge;

			fixed4 frag(v2f i) : SV_Target
			{
				clip(tex2D(_DissolveTex, i.uv) + smoothstep(_DissolveEdge - 0.1f, _DissolveEdge + 0.1f, i.loc_pos.x + 0.5f) * 2.f - 1.f);
				return i.faceColor;
			}
			ENDCG
		}
	}
	SubShader
	{
		Tags {"RenderType" = "OpaqueBoid" }
		Pass
		{
			Cull Off
			ZWrite On

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			#pragma target 4.5
			#include "UnityCG.cginc"

			struct Boid {
				float3 position;
				float3 velocity;
				float3 direction;
				float3 up;
				float4x4 rotation_matrix;
				float color;
			};

#if SHADER_TARGET >= 45
			StructuredBuffer<Boid> _boids;
#endif
			float4 _worldPosition;
			float _farWall;

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 loc_pos : TEXCOORD0;
				float cam_dist : TEXCOORD1;
				fixed4 faceColor : COLOR;
			};

			v2f vert(appdata v, uint instanceID : SV_InstanceID)
			{
#if SHADER_TARGET >= 45
				Boid boid = _boids[instanceID];
#else
				Boid boid;
#endif
				float scale = 100.f;
				float4x4 scale_matrix = float4x4(
					float4(scale, 0.f, 0.f, 0.f),
					float4(0.f, scale, 0.f, 0.f),
					float4(0.f, 0.f, scale, 0.f),
					float4(0.f, 0.f, 0.f, 1.f)
				);

				v2f o;
				o.loc_pos = mul(mul(boid.rotation_matrix, scale_matrix), float4(v.vertex.xyz, 1.f)) + float4(boid.position.xyz, 1.f);
				o.pos = mul(UNITY_MATRIX_VP, o.loc_pos + _worldPosition);
				o.cam_dist = length(WorldSpaceViewDir(o.loc_pos + _worldPosition));

				fixed3 faceVal = 0.05 * fmod(mul(unity_ObjectToWorld, float4(0, 0, 0, 1)), 10) + 0.25 * (boid.direction + 1);
				o.faceColor = fixed4(faceVal, 1);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				clip(dot(i.loc_pos, i.loc_pos) - 20.f);
				clip(i.cam_dist - 20.f);
				return i.faceColor;
			}
			ENDCG
		}
	}
	SubShader
	{
		Tags { "RenderType" = "Portal" }
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				return float4(0,0,0,1);
			}
			ENDCG
		}
	}
    FallBack "Diffuse"
}
