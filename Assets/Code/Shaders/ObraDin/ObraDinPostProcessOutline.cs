﻿using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess(typeof(ObraDinPostProcessOutlineRenderer), PostProcessEvent.BeforeStack, "Custom/ObraDinOutline")]
public sealed class ObraDinPostProcessOutline : PostProcessEffectSettings
{
    [Range(0f, 1f), Tooltip("Outline offset")]
    public Vector2Parameter outlineOffset = new Vector2Parameter { value = new Vector2(0f, 0f) };
    [Range(1f, 10f), Tooltip("Outline width")]
    public FloatParameter outlineWidth = new FloatParameter { value = 1f };
}

public sealed class ObraDinPostProcessOutlineRenderer : PostProcessEffectRenderer<ObraDinPostProcessOutline>
{
    private Shader postShader_;

    public override void Init() {
        postShader_ = Shader.Find("Hidden/ObraDinOutlinesShader");
        base.Init();
    }

    public override void Render(PostProcessRenderContext context) {
        if (!ObraDinCameraController.OutlinesTexture) {
            Debug.Log("Outlines camera not ready yet, halt rendering");
            return;
        }

        PropertySheet sheet = context.propertySheets.Get(postShader_);
        sheet.properties.SetTexture("_OutlineTex", ObraDinCameraController.OutlinesTexture);
        sheet.properties.SetVector("_OutlineOffset", settings.outlineOffset);
        sheet.properties.SetFloat("_OutlineWidth", settings.outlineWidth);

        context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
    }
}