﻿Shader "Hidden/ObraDinOutlinesShader"
{
	HLSLINCLUDE

		#include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"

		TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
		TEXTURE2D_SAMPLER2D(_OutlineTex, sampler_OutlineTex);
		half4 _MainTex_TexelSize;
		half4 _OutlineTex_TexelSize;
		float2 _OutlineOffset;
		float _OutlineWidth;

		float4 Frag(VaryingsDefault i) : SV_Target
		{
			float halfWidthFloor = floor(_OutlineWidth * 0.5);
			float halfWidthCeil = ceil(_OutlineWidth * 0.5);
			float2 texcoord = i.texcoord + float2(_OutlineOffset.x, _OutlineOffset.y);
			float4 colSW = SAMPLE_TEXTURE2D(_OutlineTex, sampler_OutlineTex, texcoord - halfWidthFloor * float2(_OutlineTex_TexelSize.x, _OutlineTex_TexelSize.y));
			float4 colNE = SAMPLE_TEXTURE2D(_OutlineTex, sampler_OutlineTex, texcoord + halfWidthCeil * float2(_OutlineTex_TexelSize.x, _OutlineTex_TexelSize.y));
			float4 colSE = SAMPLE_TEXTURE2D(_OutlineTex, sampler_OutlineTex, texcoord +
				float2(_OutlineTex_TexelSize.x * halfWidthCeil, -_OutlineTex_TexelSize.y * halfWidthFloor));
			float4 colNW = SAMPLE_TEXTURE2D(_OutlineTex, sampler_OutlineTex, texcoord +
				float2(-_OutlineTex_TexelSize.x * halfWidthFloor, _OutlineTex_TexelSize.y * halfWidthCeil));

			float4 colTrue = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoord);
			float4 colTrueSW = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoord - halfWidthFloor * float2(_MainTex_TexelSize.x, _MainTex_TexelSize.y));
			float4 colTrueNE = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoord + halfWidthCeil * float2(_MainTex_TexelSize.x, _MainTex_TexelSize.y));
			float4 colTrueSE = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoord +
				float2(_MainTex_TexelSize.x * halfWidthCeil, -_MainTex_TexelSize.y * halfWidthFloor));
			float4 colTrueNW = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoord +
				float2(-_MainTex_TexelSize.x * halfWidthFloor, _MainTex_TexelSize.y * halfWidthCeil));

			float colDif = step(0.005, sqrt(pow(distance(colNE, colSW), 2) + pow(distance(colNW, colSE), 2)));
			float avgTrueCol = step(0.1f, 0.25f * (colTrueSW.x + colTrueNE.x + colTrueSE.x + colTrueNW.x));

			// just the lines:
			// return step(0.01f, colDif * float4(1.f, 1.f, 1.f, 1.f));
			return (1 - colDif) * colTrue +
				colDif * (1 - avgTrueCol) * float4(1, 1, 1, 1);
		}

	ENDHLSL
    SubShader
    {
		Cull Off
		ZWrite Off
		ZTest Always

		Pass
		{
			HLSLPROGRAM

				#pragma vertex VertDefault
				#pragma fragment Frag

			ENDHLSL
		}
    }
}
