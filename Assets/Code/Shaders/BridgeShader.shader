﻿Shader "Unlit/BridgeShader"
{
	Properties
	{
		_Color("Color", Color) = (1, 1, 1, 1)
		_DissolveTex("Dissolve texture", 2D) = "white" {}
		_DissolveEdge("Dissolve edge", Range(0, 1.1)) = 0.0
	}

		SubShader
		{
			Tags {"RenderType" = "BridgeDissolve"}

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
					float3 loc_pos : TEXCOORD1;
				};

				sampler2D _DissolveTex;
				float4 _DissolveTex_ST;

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.loc_pos = v.vertex;
					o.uv = TRANSFORM_TEX(float2(v.uv.x - _Time.x, v.uv.y), _DissolveTex);
					return o;
				}

				fixed4 _Color;
				float _DissolveEdge;

				fixed4 frag(v2f i) : SV_Target
				{
					clip(tex2D(_DissolveTex, i.uv) + smoothstep(_DissolveEdge - 0.1f, _DissolveEdge + 0.1f, i.loc_pos.x + 0.5f) * 2.f - 1.f);
					return _Color;
				}
				ENDCG
			}
		}
}
