﻿Shader "Custom/ColorTransparent"
{
    Properties
    {
		_Color("Color", Color) = (1, 1, 1, 1)
    }

	SubShader
	{
		Tags {"Queue" = "Transparent" "RenderType" = "Transparent"}
		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			ZWrite Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase

			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal: NORMAL;
				float4 color : COLOR;
			};
	 
			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 normal : TEXCOORD0;
				float4 color : COLOR;
			};

			fixed4 _Color;

			v2f vert (appdata v) {
				v2f o;

				o.pos = UnityObjectToClipPos(v.vertex);
				o.normal = UnityObjectToWorldNormal(v.normal);
				o.color = v.color;

				return o;
			}

			fixed4 frag (v2f i) : SV_Target {
				return float4(_Color.xyz, i.color.x);
			}
			ENDCG
		}
	}
    FallBack "Diffuse"
}
