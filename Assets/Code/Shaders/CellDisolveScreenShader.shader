﻿Shader "Custom/CellDisolveScreenShader"
{
    Properties
    {
		_CellBorder("Cell Border", Range(0, 1)) = 0.5
		_CellSmoothnes("Cell Smoothnes", Range(0, 1)) = 0.1
		_Color("Color", Color) = (1, 1, 1, 1)
		_DissolveTex("Dissolve texture", 2D) = "white" {}
		_DissolveTreshold("Dissolve treshold", float) = 0.0
    }

	SubShader
	{
		Tags
		{ "RenderType" = "OpaqueDisolveScreen" }

		Pass
		{
			Tags { "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase

			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal: NORMAL;
			};
	 
			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 screen_pos : TEXCOORD0;
				float3 normal : NORMAL;
				LIGHTING_COORDS(2, 3)
			};

			v2f vert (appdata v) {
				v2f o;

				o.pos = UnityObjectToClipPos(v.vertex);
				o.normal = UnityObjectToWorldNormal(v.normal);
				o.screen_pos = ComputeScreenPos(v.vertex);
				TRANSFER_VERTEX_TO_FRAGMENT(o);

				return o;
			}

			sampler2D _DissolveTex;
			float4 _DissolveTex_ST;
			float _DissolveTreshold;
			float _CellBorder;
			float _CellSmoothnes;
			fixed4 _Color;

			fixed4 frag (v2f i) : SV_Target {
				clip(tex2D(_DissolveTex, i.screen_pos) - _DissolveTreshold);

				float toon = smoothstep(_CellBorder - 0.5*_CellSmoothnes, _CellBorder + 0.5*_CellSmoothnes, dot(i.normal, _WorldSpaceLightPos0.xyz));

				return _Color * _LightColor0 * toon * LIGHT_ATTENUATION(i);
			}
			ENDCG
		}
			
		Pass
		{
			Tags { "LightMode" = "ForwardAdd" }
			BlendOp Max

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdadd_fullshadows

			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal: NORMAL;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 normal : NORMAL;
				float4 world_pos : TEXCOORD0;
				float4 screen_pos : TEXCOORD1;
				LIGHTING_COORDS(3, 4)
			};

			v2f vert(appdata v) {
				v2f o;

				o.pos = UnityObjectToClipPos(v.vertex);
				o.normal = UnityObjectToWorldNormal(v.normal);
				o.world_pos = mul(unity_ObjectToWorld, v.vertex);
				o.screen_pos = ComputeScreenPos(o.pos);
				TRANSFER_VERTEX_TO_FRAGMENT(o);

				return o;
			}

			sampler2D _DissolveTex;
			float4 _DissolveTex_ST;
			float _DissolveTreshold;
			float _CellBorder;
			float _CellSmoothnes;
			fixed4 _Color;

			fixed4 frag(v2f i) : SV_Target {
				clip(tex2D(_DissolveTex, i.screen_pos) - _DissolveTreshold);

				float3 lightDir = _WorldSpaceLightPos0.w == 0 ? _WorldSpaceLightPos0.xyz : normalize(_WorldSpaceLightPos0.xyz - i.world_pos.xyz);
				float toon = step(0.001, dot(i.normal, lightDir));
#ifdef POINT
				float lightAttenAtPoint = length(mul(unity_WorldToLight, i.world_pos).xyz);
				float lightDistToPoint = length(_WorldSpaceLightPos0.xyz - i.world_pos.xyz);
				toon *= 1 - step(lightDistToPoint / lightAttenAtPoint, length(_WorldSpaceLightPos0.xyz - i.world_pos.xyz));
#endif

				return _Color * _LightColor0 * toon * SHADOW_ATTENUATION(i);
			}
			ENDCG
		}
	}
    FallBack "Diffuse"
}
