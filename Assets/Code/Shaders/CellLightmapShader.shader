﻿Shader "Custom/CellLightmapShader"
{
    Properties
    {
		_CellBorder("Cell Border", Range(0, 1)) = 0.5
		_CellSmoothnes("Cell Smoothnes", Range(0, 1)) = 0.1
		_Color("Color", Color) = (1, 1, 1, 1)
    }

	SubShader
	{
		Tags { "RenderType" = "Opaque" }
	
		Pass
		{
			Name "META"
			Tags {"LightMode" = "Meta"}
			Cull Off
			CGPROGRAM

			#include"UnityStandardMeta.cginc"

			#pragma vertex vert_meta
			#pragma fragment frag_meta
			ENDCG
		}

		Pass
		{
			Tags { "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase

			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD1;
			};
	 
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				LIGHTING_COORDS(1, 2)
			};

			fixed4 _Color;
			float _CellBorder;
			float _CellSmoothnes;

			v2f vert (appdata v) {
				v2f o;

				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv.xy * unity_LightmapST.xy + unity_LightmapST.zw;
				TRANSFER_VERTEX_TO_FRAGMENT(o);

				return o;
			}

			fixed4 frag (v2f i) : SV_Target {
				half4 lightmap = half4(DecodeLightmap(UNITY_SAMPLE_TEX2D(unity_Lightmap, i.uv.xy)), 1.0);

				return _Color * smoothstep(_CellBorder - 0.5*_CellSmoothnes, _CellBorder + 0.5*_CellSmoothnes, lightmap.x) * LIGHT_ATTENUATION(i);
			}
			ENDCG
		}
	}
    FallBack "Diffuse"
}
