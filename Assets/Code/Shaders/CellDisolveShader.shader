﻿Shader "Custom/CellDisolveShader"
{
	Properties
	{
		_CellBorder("Cell Border", Range(0, 1)) = 0.5
		_CellSmoothnes("Cell Smoothnes", Range(0, 1)) = 0.1
		_Color("Color", Color) = (1, 1, 1, 1)
		_DissolveTex("Dissolve texture", 2D) = "white" {}
		_DissolveTreshold("Dissolve treshold", Range(0, 1)) = 0.0
	}

	SubShader
	{
		Tags {"RenderType" = "Dissolve"}

		Pass
		{
			Tags { "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase

			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal: NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 normal : TEXCOORD0;
				float2 uv : TEXCOORD1;
				LIGHTING_COORDS(2, 3)
			};

			float _CellBorder;
			float _CellSmoothnes;
			sampler2D _DissolveTex;
			float4 _DissolveTex_ST;

			v2f vert(appdata v) {
				v2f o;

				o.pos = UnityObjectToClipPos(v.vertex);
				o.normal = UnityObjectToWorldNormal(v.normal);
				o.uv = TRANSFORM_TEX(v.uv, _DissolveTex);
				TRANSFER_VERTEX_TO_FRAGMENT(o);

				return o;
			}

			fixed4 _Color;
			float _DissolveTreshold;

			fixed4 frag(v2f i) : SV_Target {
				clip(tex2D(_DissolveTex, i.uv) - _DissolveTreshold);

				float toon = smoothstep(_CellBorder - 0.5*_CellSmoothnes, _CellBorder + 0.5*_CellSmoothnes, dot(i.normal, _WorldSpaceLightPos0.xyz));

				return _Color * _LightColor0 * toon * LIGHT_ATTENUATION(i);
			}
			ENDCG
		}

		Pass
		{
			Tags { "LightMode" = "ForwardAdd" }
			BlendOp Max

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdadd_fullshadows

			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal: NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 normal : TEXCOORD0;
				float4 worldPos : TEXCOORD1;
				float2 uv : TEXCOORD2;
				LIGHTING_COORDS(3, 4)
			};

			float _CellBorder;
			float _CellSmoothnes;
			sampler2D _DissolveTex;
			float4 _DissolveTex_ST;

			v2f vert(appdata v) {
				v2f o;

				o.pos = UnityObjectToClipPos(v.vertex);
				o.normal = UnityObjectToWorldNormal(v.normal);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _DissolveTex);
				TRANSFER_VERTEX_TO_FRAGMENT(o);

				return o;
			}

			fixed4 _Color;
			float _DissolveTreshold;

			fixed4 frag(v2f i) : SV_Target {
				clip(tex2D(_DissolveTex, i.uv) - _DissolveTreshold);

				float3 lightDir = _WorldSpaceLightPos0.w == 0 ? _WorldSpaceLightPos0.xyz : normalize(_WorldSpaceLightPos0.xyz - i.worldPos.xyz);
				float toon = step(0.001, dot(i.normal, lightDir));
#ifdef POINT
				float lightAttenAtPoint = length(mul(unity_WorldToLight, i.worldPos).xyz);
				float lightDistToPoint = length(_WorldSpaceLightPos0.xyz - i.worldPos.xyz);
				toon *= 1 - step(lightDistToPoint / lightAttenAtPoint, length(_WorldSpaceLightPos0.xyz - i.worldPos.xyz));
#endif

				return _Color * _LightColor0 * toon * SHADOW_ATTENUATION(i);
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
