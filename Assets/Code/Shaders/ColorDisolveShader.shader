﻿Shader "Unlit/ColorDisolveShader"
{
    Properties
    {
		_Color("Color", Color) = (1, 1, 1, 1)
		_DissolveTex("Dissolve texture", 2D) = "white" {}
		_DissolveTreshold("Dissolve treshold", Range(0, 1)) = 0.0
    }

    SubShader
    {
		Tags {"RenderType" = "Dissolve"}

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _DissolveTex;
            float4 _DissolveTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _DissolveTex);
                return o;
            }

			fixed4 _Color;
			float _DissolveTreshold;

            fixed4 frag (v2f i) : SV_Target
            {
				clip(tex2D(_DissolveTex, i.uv) - _DissolveTreshold);
                return _Color;
            }
            ENDCG
        }
    }
}
