﻿Shader "Custom/Particles"
{
	Properties
	{
		// Properties from PS
	}

	SubShader
	{
		Tags
		{ "RenderType" = "Opaque" }

		Pass
		{
			Tags { "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase

			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				fixed4 color : COLOR;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				fixed4 color : COLOR;
			};

			fixed4 _Color;

			v2f vert(appdata v) {
				v2f o;

				o.pos = UnityObjectToClipPos(v.vertex);
				o.color = v.color;

				return o;
			}

			fixed4 frag(v2f i) : SV_Target {

				return i.color;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
